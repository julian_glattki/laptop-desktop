package spiel;

import java.util.ArrayList;

public class Player {
	private int money;
	private ArrayList<Card> playerCards;
	
	// Konstruktoren
	public Player() {
		this.money = 0;
		this.playerCards = new ArrayList<Card>();
	}
	public Player(int money) {
		this.money = money;
		this.playerCards = new ArrayList<Card>();
	}
	
	// Getter/Setter money
	public int getMoney() {
		return this.money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	
	// Getter playerCards
	public ArrayList<Card> getPlayerCards() {
		return this.playerCards;
	}
	
	public void addRandomCard() {
		this.playerCards.add(Card.generateRandomCard());
	}
	
	/* Prints all cards in the hand of the player and gives the sum of the ranks */
	public void printPlayerCards() {
		for(Card card : this.playerCards) {
			System.out.println(card.toString());
		}
		System.out.println("Summe: " + this.sumOfPlayerCardRanks());
	}
	/* Adds all ranks of the player's cards and evalutes the sum */
	public int sumOfPlayerCardRanks() {
		int sum = 0;
		for(Card card : this.playerCards) {
			sum += card.getCardValue();
		}
		return sum;
	}
	
}
