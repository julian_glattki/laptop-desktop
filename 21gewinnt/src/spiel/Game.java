package spiel;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Game {
	public static Scanner in = new Scanner(System.in);
	private int pot; 
	public static void main(String[] args) throws InterruptedException {
		willkommensnachricht(); 
		Player spieler1 = new Player();
		Bank bank1 = new Bank(10000);
		
		System.out.print("Bitte gewünschte Startchips eingeben (Bank hat 10000):");
		spieler1.setMoney(Integer.parseInt(in.nextLine()));
		System.out.println("\n\n");
		
		System.out.println("Spiel beginnt!");
		TimeUnit.SECONDS.sleep(3);
		spieler1.addRandomCard();
		bank1.addRandomCard();
		
		printBoard(spieler1, bank1);
		
		
		
	}
	/* Gibt Willkommensnachricht aus */
	public static void willkommensnachricht() {
		System.out.println("Willkommen zu 21 gewinnt!");
		System.out.println("Regeln:\nEin Einsatz wird platziert. Der Spieler erhält eine Karte. Die Bank erhält eine Karte."
				+ "\nNun kann der Spieler weitere Karten anfordern oder aussteigen."
				+ "\nDer Spieler muss 21 Punkte erreichen, um den vierfachen Einsatz zu erhalten. Übertrifft er 21 erhält er nichts."
				+ "\nSteigt der Spieler aus zieht die Bank eine Karte. Ist der Wert der Karten unter 17 muss die Bank eine weitere"
				+ "\nKarte ziehen. Liegt der Wert der Karten zwischen 17 und 21 verliert der Spieler seinen Einsatz. Liegt der Wert"
				+ "\nüber 21 erhält der Spieler den doppelten Einsatz.");
		System.out.println("Bereit? [Enter drücken]");
		String spielerBereit = in.nextLine();
		System.out.println("\n\n\n\n\n");
	}
	
	public static void printBoard(Player player, Bank bank) {
		System.out.println("Ihre Karten:");
		player.printPlayerCards();
		System.out.println("Meine Karten:");
		bank.printBankCards();
		
	}
	
	public static void printMoney(Player player, Bank bank) {
		System.out.println("Ihr Geld:" + player.getMoney());
		System.out.println("Mein Geld:" + bank.getMoney());
	}
	
	
	
}
