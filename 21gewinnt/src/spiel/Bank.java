package spiel;

import java.util.ArrayList;

public class Bank {
	private int money;
	private ArrayList<Card> bankCards;
	
	// Konstruktoren
	public Bank() {
		this.money = 0;
		this.bankCards = new ArrayList<Card>();
	}
	public Bank(int money) {
		this.money = money;
		this.bankCards = new ArrayList<Card>();
	}
	
	// Getter/Setter money
	public int getMoney() {
		return this.money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	
	public void addRandomCard() {
		this.bankCards.add(Card.generateRandomCard());
	}
	
	
	// Getter playerCards
	public ArrayList<Card> getBankCards() {
		return this.bankCards;
	}
	/* Prints all cards in the hand of the bank and gives the sum of the ranks */
	public void printBankCards() {
		for(Card card : this.bankCards) {
			System.out.println(card.toString());
		}
		System.out.println("Summe: " + this.sumOfBankCardRanks());
	}
	/* Adds all ranks of the bank's cards and evalutes the sum */
	public int sumOfBankCardRanks() {
		int sum = 0;
		
		for(Card card : this.bankCards) {
			sum += card.getCardValue();
		}
		
		return sum;
	}
	
}
