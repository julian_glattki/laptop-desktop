package spiel;

import java.util.Random;

public class Card {
	private int rank; 
	private int suit;
	private int cardValue;
	
	public static final String[] RANKS = {null, null, "2", "3", "4", "5", "6", "7", "8", "9", "Ten", "Jack", "Queen", "King", "Ace"};
	public static final String[] SUITS = {"Clubs", "Diamonds", "Hearts", "Spades"};
	public static final int[] CARDVALUE = {0, 0, 2,3,4,5,6,7,8,9,10,10,10,10,11};

	// Konstruktoren
	public Card() {
		this.rank = 0;
		this.suit = 0;
		this.cardValue = 0;
	}
	public Card(int rank, int suit, int cardValue) {
		this.rank = rank;
		this.suit = suit;
		this.cardValue = cardValue;
	}
	
	// Getters
	public int getRank() {
		return this.rank;
	}
	public int getSuit() {
		return this.suit;
	}
	public int getCardValue() {
		return this.cardValue;
	}
	// toString
	
	public String toString() {
		return RANKS[this.rank] + " of " + SUITS[this.suit];
	}
	// print
	
	public void printCard() {
		System.out.println(RANKS[this.rank] + " of " + SUITS[this.suit]);
	}
	
	/* Generates a random card in the range of a standard 52 card deck.
	 * @return new Card object with a random integer for rank and a random integer for suit
	 */
	public static Card generateRandomCard() {
		Random rnd = new Random();
		int randomRank = rnd.nextInt(RANKS.length)+2; // Generates a random number from 2(included) to RANKS.length == 15 (excluded)
		int randomSuit = rnd.nextInt(SUITS.length); // Generates a random number from 0(included) to SUITS.length == 4 (excluded)
		return new Card(randomRank, randomSuit, CARDVALUE[randomRank]);
	}
	

}
