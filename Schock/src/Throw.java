
public class Throw {
	private int noOfDices;
	private Dice[] dices;
	
	public Throw(int noOfDices) {
		this.noOfDices = noOfDices;
		
		for(int i = 0; i < noOfDices; i++) {
			dices[i] = new Dice();
		}
	}

}
