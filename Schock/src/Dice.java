
public class Dice {
	private int currentTop;
	private boolean locked;
	
	static final int SIDES = 6;
	
	public Dice() {
		this.currentTop = (int) (Math.random()*SIDES + 1);
		this.locked = false;
	}

	public int getCurrentTop() {
		return currentTop;
	}
	
	public void setCurrentTop(int currentTop) {
		this.currentTop = currentTop;
	}
	
	public boolean getLocked() {
		return locked;
	}
	
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
	
	public void rollDice() {
		this.currentTop = (int) (Math.random()*SIDES + 1);
	}




	
}
