import java.util.Scanner;

public class MainMenu {
	static Scanner sc = new Scanner(System.in);


	public static void main(String[] args) {

		int noOfPlayers = 0;

		System.out.println("Welcome!");
		System.out.print("Please enter the number of players: ");
		noOfPlayers = enterNumberBiggerThanZero();
		System.out.println("Number of players: " + noOfPlayers);
		String[] names = enterPlayerNames(noOfPlayers);
		System.out.print("Please enter the number of dices you want to play with: ");
		int noOfDices = enterNumberBiggerThanZero();

		new Game(noOfPlayers, names, noOfDices);


	}

	public static int enterNumberBiggerThanZero() {
		Integer number = -1;

		while(number <= 0) {
			number = enterNumber();
			if(number <= 0) {
				System.out.println("Please enter a number bigger than zero!");
			}
		}

		return number;
	}
	public static int enterNumber() {
		int number = 0; 
		
		while(!sc.hasNextInt()) {
			System.out.println("Invalid! Please enter a valid number!");
			sc.next();
		}
		
		number = sc.nextInt();
		
		sc.nextLine();
		
		return number;

	}

	public static String[] enterPlayerNames(int noOfPlayers) {
		String[] names = new String[noOfPlayers];

		for(int i = 0; i < noOfPlayers; i++) {
			System.out.print("Player " + (i+1) + ": ");
			names[i] = sc.nextLine();
			System.out.println("Player " + (i+1) + " is " + names[i]);

		}
		return names;
	}
}