import java.util.ArrayList;

public class Player {
	private String name;
	private ArrayList<Integer> lastThrow;
	
	public Player(String name) {
		this.name = name;
		this.lastThrow = new ArrayList<Integer>();
	}
	
	public String lastThrowToString() {
		String lastThrowString = "";
		for(int element : this.lastThrow) {
			lastThrowString += element + "|";
		}
		
		return lastThrowString;
	}
	public void setLastThrowValues(int[] thrownValues) {
		for(int i = 0; i < thrownValues.length; i++) {
			this.lastThrow.add(thrownValues[i]);
		}
	}
	
	public String getName() {
		return this.name;
	}

}
