public class Board {
	private Dice[] dicesOnBoard;
	private int noOfDices;
	
	public Board(int noOfDices) {
		this.noOfDices = noOfDices;
		this.dicesOnBoard = new Dice[this.noOfDices];
		
		for(int i = 0; i < noOfDices; i++) {
			dicesOnBoard[i] = new Dice();
			dicesOnBoard[i].setCurrentTop(1);
		}
	}
	
	public Dice[] getDicesOnBoard() {
		return this.dicesOnBoard;
	}
	public void lockDice(int position) {
		dicesOnBoard[position].setLocked(true);
	}
	
	public void resetBoard() {
		for(Dice element : dicesOnBoard) {
			element.setLocked(false);
		}
	}
	
	public boolean checkKetscherAktion() {
		for(Dice element : dicesOnBoard) {
			if(element.getCurrentTop() != 1) {
				return false;
			}
		}
		return true;
	}
	public void rollDices() {
		for(Dice element : dicesOnBoard) {
			if(element.getLocked() == false) {
				element.rollDice();
			}
		}
	}
	
	public int[] toIntArray() {
		int[] boardAsInt = new int[noOfDices];
		
		for(int i = 0; i < noOfDices; i++) {
			boardAsInt[i] = dicesOnBoard[i].getCurrentTop();
		}
		
		return boardAsInt;
	}
	
	public String toString() {
		String board = "";
		String positions = ""; 
		String linebreak = ""; 
		int counter = 1; 
		
		
		for(Dice element : dicesOnBoard) {
			board += element.getCurrentTop()+"|";
			positions += counter + "|";
			linebreak += "XX";
			counter++;
		}
		
		return "\n" + board + "\n" + linebreak + "\n" + positions + "\n";
	}
	
	public boolean turnSixPossible() {
		int count = 0; 
		for(Dice element : dicesOnBoard) {
			if(element.getCurrentTop() == 6) {
				count++;
			}
			if(count == 2) {
				return true;
			}
		}
		
		return false;
	}
	
	public void turnSix() {
		for(Dice element : dicesOnBoard) {
			if(element.getCurrentTop() == 6) {
				element.setCurrentTop(1);
				break;
			}
		}
	}
}
