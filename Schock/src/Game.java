public class Game {
	private Player[] players;
	private Board board;

	boolean play = true;
	int playerTurn = 0; 
	int round = 0; 
	int attempts = 0; 

	public Game(int noOfPlayers, String[] playerNames, int noOfDices) {
		this.board = new Board(noOfDices);
		this.players = new Player[playerNames.length];

		for(int i = 0; i < noOfPlayers; i++) {
			this.players[i] = new Player(playerNames[i]);
		}

		run();
	}

	public void run() {
		while(this.play) {
			newRound();

			while(playerTurn < this.players.length) {
				printPlayerTurn();
				attempts = 0;
				while(attempts < 3) {
					this.board.rollDices();
					System.out.println(this.board.toString());

					if(this.board.checkKetscherAktion()) {
						System.out.println("KETSCHER AKTION!!!!");
					}

					while(this.board.turnSixPossible()) {
						attempts--;
						System.out.println("Your attempts were reduced by one!");
						System.out.println("Do you want to turn a six over?");
						boolean turnOver = enterYesNo();
						if(!turnOver) {
							break;
						}
						if(turnOver) {
							this.board.turnSix();
						}
						System.out.println(this.board.toString());
					}

					if(attempts < 2) {
						System.out.println("Do you want to lock something? (Y/N)");
						boolean userLock = enterYesNo();

						while(userLock) {
							lockDice();
							System.out.println("Do you want to lock something else?");
							userLock = enterYesNo();
						}
					}
					if(attempts < 2) {
						System.out.println("Do you want to pass the dices to the next player?");
						boolean done = enterYesNo();
						if(done) {
							break;
						}
					}
					attempts++;
				}
				this.players[playerTurn].setLastThrowValues(this.board.toIntArray());
				System.out.println(this.players[playerTurn].getName() + " your final board is \n" + this.players[playerTurn].lastThrowToString() +
						"\n________________________________________________________");
				playerTurn++;
			}


			System.out.println("Round " + this.round + " is over.");
			for(int i = 0; i < this.players.length; i++) {
				System.out.println(players[i].getName() + " has " + players[i].lastThrowToString());
			}
			round++;
			playerTurn = 0; 
			this.board.resetBoard();
		}
	}


	public void lockDice() {
		System.out.println("Please enter the position of the dice you want to lock: ");
		int lock = enterLockedDice();
		this.board.getDicesOnBoard()[lock].setLocked(true);
	}
	public int enterLockedDice() {

		int lock = MainMenu.enterNumberBiggerThanZero()-1;

		if(lock >= this.board.getDicesOnBoard().length) {
			System.out.println("Please enter a valid number!");
			enterLockedDice();
		}
		else if(lock < 0) {
			System.out.println("Please enter a valid number!");
			enterLockedDice();
		}

		return lock;
	}
	public void newRound() {
		System.out.println("________________________________________________________");
		System.out.println("Round " + this.round);
	}

	public void printPlayerTurn() {
		System.out.println(players[playerTurn].getName() + ", it's your turn!");

	}

	public boolean enterYesNo() {
		String userInput = MainMenu.sc.nextLine().toUpperCase();

		if(userInput.equals("Y")) {
			return true;
		}
		else if(userInput.equals("N")) {
			return false;
		}
		else {
			System.out.println("Please enter either 'Y' or 'N'");
			enterYesNo();
		}

		return false;
	}
}
