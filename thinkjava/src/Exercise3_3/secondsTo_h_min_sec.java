package Exercise3_3;

import java.util.Scanner;

public class secondsTo_h_min_sec {
	
	public static void main(String[] args) {
		int seconds;
		int hours; 
		int minutes;
		int remainingSeconds;
		
		
		System.out.print("Bitte geben Sie die Sekunden an, die in Stunden, Minuten und Sekunden umgewandelt werden sollen: ");
		Scanner in = new Scanner(System.in);
		seconds = in.nextInt();
		in.close();
		
		hours = seconds / 3600;
		minutes = (seconds % 3600)/ 60;
		remainingSeconds = (seconds % 3600)%60;
		
		System.out.printf("%d second(s) = %d hour(s), %d minute(s), and %d second(s)", seconds, hours, minutes, remainingSeconds);
		
		
	
	}

}
