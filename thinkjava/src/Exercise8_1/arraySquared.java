package Exercise8_1;

public class arraySquared {

	public static void main(String[] args) {
		int[] array = {1,2,4,8,16,32,64,128};
		int [] squaredArray = squareArray(array,4);
		
		for(int i = 0; i < squaredArray.length; i++) {
			System.out.println(squaredArray[i]);
		}
		
		
	}
	public static int[] squareArray(int[] array, double n) {
		int[] squaredArray = new int[array.length];
		for(int i = 0; i < array.length; i++) {
			squaredArray[i] = (int) Math.pow(array[i],n);
		}

		return squaredArray;
	}
	
	public static int[] histogram(int[] array, int count) {
		int counter[] = new int[count];
		for(int element:array) {
			counter[element]++;
		}
		return counter;
	}
}
