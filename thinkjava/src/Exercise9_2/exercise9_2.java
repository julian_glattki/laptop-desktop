package Exercise9_2;


public class exercise9_2 {

	public static void main(String[] args) {
		
		String zeichenkette = "hallo wie geht es dir heute ich w�nsche dir einen sch�nen tag";
		int alphabet = 65;
//		int[] histogram = hist(zeichenkette);
		
//		for(int i = 0; i < 26; i++) {
//			System.out.print((char) alphabet +  " : ");
//			System.out.println(histogram[i]);
//			alphabet++;
//		}
		
		
		int[] histogram2= histSchöner(zeichenkette);
		for(int i = 0; i < 27; i++) {
			if(i < 26) {
				System.out.print((char) alphabet + " : ");
				System.out.println(histogram2[i]);
				alphabet++;
			}
			
			else {
				System.out.print("Sonderzeichen : ");
				System.out.println(histogram2[26]);
			}
		}

	}
	
	public static int[] hist(String zeichenkette) {
		char[] zeichen = zeichenkette.toUpperCase().toCharArray();
		int menge[] = new int[26];
		
		for(int i = 0; i < zeichen.length; i++) {
			char c = zeichen[i];
			if(c >= 65 && c<=90) {
				menge[c-65]++;
			}		
		}
		
	return menge;
	}
	
	
	public static int[] histSchöner(String zeichenkette) {
		char[] characters = zeichenkette.toUpperCase().toCharArray();
		int[] menge = new int[27];
		
		for(char character : characters) {
			if(character >= 65 && character <= 90) {
				menge[character-65]++;
			}
			else if(character != 32) {
				menge[26]++;
			}
		}
		return menge;
	}

}