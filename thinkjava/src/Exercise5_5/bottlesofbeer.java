package Exercise5_5;

public class bottlesofbeer {

	public static void main(String[] args) {
		int verse = 3500;
		start(verse);

	}
	public static void start(int verse) {
		if(verse != 0) {
		System.out.println(verse + " bottles of beer on the wall,\r\n" + 
				verse +" bottles of beer,\r\n" + 
				"ya� take one down, ya� pass it around");
		
		decrease(verse);
		}
		else {
			System.out.println("No bottles of beer on the wall,\r\n" + 
					"no bottles of beer,\r\n" + 
					"ya� can�t take one down, ya� can�t pass it around,\r\n" + 
					"�cause there are no more bottles of beer on the wall!");
		}
		
		
	}
	
	public static void decrease(int verse) {
		verse--;
		System.out.println(verse + " bottles of beer on the wall.\n");
		start(verse);
	}
}
