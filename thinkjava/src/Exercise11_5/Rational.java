package Exercise11_5;

public class Rational {
	private int numerator;
	private int denominator;

	public Rational() {
		this.numerator = 0;
		this.denominator = 1; 
	}
	public Rational(int numerator, int denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
	}
	public String toString() {
		return this.numerator + "/" + this.denominator;
	}
	public void printRational() {
		System.out.println(this.numerator + "/" + this.denominator);
	}

	public void negate() {
		if(this.numerator < 0 && this.denominator > 0 ) {
			this.numerator = -this.numerator;
		}
		else if(this.numerator > 0 && this.denominator < 0) {
			this.denominator = -this.denominator;
		}
		else if(this.numerator > 0 && this.denominator > 0) {
			this.numerator = -this.numerator;
		}
		else {
			this.numerator = -this.numerator;
		}
	}

	public void inverse() {
		int holder = this.numerator; 
		this.numerator = this.denominator;
		this.denominator = holder;
	}

	public double toDouble() {
		double numerator = this.numerator;
		double denominator = this.denominator;

		return numerator/denominator;
	}

	private int	gcd(int m,int n){
		if	(m	% n == 0) {
			return n;
		} 
		else {
			return gcd(n,m	% n);
		}
	}

	public Rational reduce() {
		int gcD = gcd(this.numerator, this.denominator);
		return new Rational(this.numerator/gcD, this.denominator/gcD);
	}
	
	public Rational add(Rational bruch) {
		Rational summe = new Rational();
		
		summe.numerator = this.numerator*bruch.denominator + bruch.numerator*this.denominator;
		summe.denominator = this.denominator * bruch.denominator;
		System.out.println(summe);
		summe = summe.reduce();
		
		return summe;
		
	}
	
public static void main(String[] args) {
		Rational eins = new Rational();
		eins.numerator = 1;
		eins.denominator = 4000;

		Rational zwei = new Rational(1,1);
		Rational drei = new Rational(-1,1);
		Rational vier = new Rational(5, 20);
		Rational fünf = new Rational(192,256);
		Rational sieben = new Rational(2, 2);

		zwei.negate();
		System.out.println(zwei.toString());

		drei.inverse();
		System.out.println(drei.toString());

		double ergebnis = vier.toDouble();
		System.out.println(ergebnis);

		Rational sechs = fünf.reduce();
		System.out.println(sechs);
		
		
		System.out.println(sieben.add(fünf));


	}

}
