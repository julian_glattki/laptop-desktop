package Exercise6_8;

public class ackermann {

	public static void main(String[] args) {
		int m = -1;
		int n = 4;
		
		System.out.println(ackermannFunktion(m,n));

	}
	public static int ackermannFunktion(int m, int n) {
		if(m == 0) {
			return n+1;
		}
		else if(m > 0 && n == 0) {
			return ackermannFunktion(m-1, 1);
		}
		else if(m > 0 && n > 0) {
			return ackermannFunktion(m-1, ackermannFunktion(m, n -1));
		}
		
		else {
			System.err.print("Fehler!");
			return(-1);
		}
	}
}
