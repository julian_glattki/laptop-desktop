package Exercise8_4;

public class indexOfMax {

	public static void main(String[] args) {

		int[] array = {1,2,3,4,10,20,30,3,4,5,6,11,75,14,15,16};

		System.out.println(array.length);
		System.out.println(findIndexOfMax(array));
		System.out.println(findIndexOfMax1(array));
	}

	public static int findIndexOfMax(int[] array) {

		int index = 0;
		int saveIndex = 0;

		for(int element : array) {
			if(array[saveIndex] > array[index]) {
				index = saveIndex;
			}
			saveIndex++;

		}

		return index;
	}

	public static int findIndexOfMax1(int[] array) {
		int index = 0;


		for(int i = 1; i < array.length; i++) {
			if(array[i] > array[index]) {
				index = i;
			}
		}
		return index;

	}
}

