package Exercise11_3;

public class Tile {
	
	private char letter;
	private int value; 
	
	// Constructor
	public Tile(char letter, int value) {
		this.letter = letter;
		this.value = value;
	}
	
	// Getter und Setter
	public char getChar() {
		return this.letter;
	}
	public int getValue() {
		return this.value;
	}
	public void setChar(char letter) {
		this.letter = letter;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	// Print
	public void printTile() {
		System.out.println("A tile with letter " + this.letter + " and value " + this.value);
	}
	
	// ToString
	public String toString() {
		return "A tile with letter " + this.letter + " and value " + this.value ;
	}
	
	// Equals
	public boolean equals(Tile tileVergleich) {
		if(this.letter == tileVergleich.letter && this.value == tileVergleich.value) {
			return true;
		}
		else
			return false;
	}
	
	
	public static void main(String[] args) {
		Tile neu = new Tile('A', 1);
		Tile neu2 = new Tile('C', 3);
		System.out.println(neu.toString());
		System.out.println(neu2.toString());
		System.out.println(neu.equals(neu2));
		
		

	}
	public static void testTile() {
		Tile test = new Tile('Z', 10);
		test.printTile();
	}
}
