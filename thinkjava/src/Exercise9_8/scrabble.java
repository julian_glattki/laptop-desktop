package Exercise9_8;

public class scrabble {

	public static void main(String[] args) {
		String word = "Hello";
		String tiles ="abcdefg";
		
		System.out.println(canSpell(word,tiles));

	}

	public static boolean canSpell(String word, String tiles) {
		word = word.toUpperCase();
		tiles = tiles.toUpperCase();
		boolean test = false;
		char[] wordCharacters = word.toCharArray();
		char[] tilesCharacters = tiles.toCharArray();


		for(char element : wordCharacters) {
			for(int i = 0; i<tiles.length(); i++) {
				if(element == tilesCharacters[i]) {
					tilesCharacters[i] = 0;
					test = true;
					break;
				}
			}
			if(test = true) {
				test = false;
				continue;
			}
			else {
				return false;}
		}

		return true;


	}
}
