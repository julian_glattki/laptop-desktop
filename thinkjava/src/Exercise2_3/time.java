package Exercise2_3;

public class time {

	public static void main(String[] args) {
		
		// 2.2
		
		int hour = 17, minute = 51, second = 32;
		
		// 2.3
									
		int secondsSinceMidnight = (hour * 3600) + (minute*60) + second;	// Manually
		System.out.print("Sekunden seit Mitternacht: ");
		System.out.println(secondsSinceMidnight);
		
		// System.out.println((hour * 60) + (minute*60) + second);			// Automatically
		
		// 2.4
		int aDayInSeconds = 24*3600;
		int secondsUntilMidnight = aDayInSeconds - secondsSinceMidnight;
		
		System.out.print("Sekunden bis Mitternacht: ");
		System.out.println(secondsUntilMidnight);
		
		
		// 2.5
		
		double percentageOfDayThatHasPassed = ((double) secondsSinceMidnight/aDayInSeconds);
		System.out.print("Prozentualer Anteil des vergangenen Tages: ");
		System.out.println(percentageOfDayThatHasPassed);

		
	
		// 2.6
	
		int hourNow = 20, minuteNow = 6, secondNow = 42;
		int timeInSecondsNow = hourNow*3600 + minuteNow * 60 + secondNow;
		
		int timePassed = timeInSecondsNow - secondsSinceMidnight;
		
		int hoursGone = (timePassed / 3600);
		int minutesGone = (timePassed%3600) / 60;
		int secondsGone = ((timePassed%3600) % 60);
		
		System.out.println(hoursGone);
		System.out.print("Zeit die f�r �bung ben�tigt wurde: ");
		System.out.println(hoursGone + ":" + minutesGone + ":" + secondsGone);
	}
}
