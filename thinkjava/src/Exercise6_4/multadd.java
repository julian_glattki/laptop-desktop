package Exercise6_4;

public class multadd {

	public static void main(String[] args) {
		double a = 4.0;
		double b = 2.0;
		double c = 3.0;
		
		
		System.out.println(multiadd(a,b,c));
		System.out.println(multiadd(0.5, Math.cos(Math.PI/4), Math.sin(Math.PI/4)));
		System.out.println(multiadd(1, Math.log(10), Math.log(20)));
		System.out.println(expSum(-1));
	}
	public static double multiadd(double a, double b, double c) {
		return a*b+c;
	}
	
	public static double expSum(double x) {
		return multiadd(x, Math.pow(Math.E, -x), Math.sqrt(1-Math.pow(Math.E, x)));
	}
}
