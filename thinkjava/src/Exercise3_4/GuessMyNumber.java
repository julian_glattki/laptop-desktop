package Exercise3_4;

import java.util.Scanner;
import java.util.Random;

public class GuessMyNumber {
	public static void main(String[] args) {
		
		int number;
		int userNumber;
		int unterschied;
		
		// Scanner
		System.out.print("Welche Nummer w�hlen Sie?");
		Scanner in = new Scanner(System.in);
		userNumber = in.nextInt();
		in.close();
	
		// Zuf�llige Zahl generieren
		Random rnd = new Random();
		number = rnd.nextInt(100)+1;
		
		// Verarbeitung
		
		System.out.println("Sie w�hlen " + userNumber);
		System.out.println("Ich habe " + number + " gew�hlt.");
		
		if(number == userNumber) {
			System.out.println("Sie haben gewonnen!");
		}
		else {
			System.out.println("Sie haben verloren!");
			unterschied = Math.abs(number-userNumber);
			System.out.println("Der Unterschied zwischen uns liegt bei " + unterschied);
		}
	}
}
