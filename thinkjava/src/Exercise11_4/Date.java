package Exercise11_4;

public class Date {
	
	private int day;
	private int month;
	private int year;
	
	public Date() {
		this.day = 1;
		this.month = 1;
		this.year = 0;
	}
	
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year; 
	}
	
	public void print() {
		System.out.println(this.day + "/" + this.month + "/" + this.year);
	}
	public static void main(String[] args) {
		Date gebbes = new Date();
		gebbes.day = 16;
		gebbes.month = 8;
		gebbes.year = 1998;
		
		Date gebbes2 = new Date(16,8,1998);
		
		gebbes.print();
		gebbes2.print();

	}

}
