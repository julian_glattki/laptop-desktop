package Exercise9_1;

public class combineTypes {

	public static void main(String[] args) {
		
		int a = 34;
		double b = 42;
		String c = "Julian";
		boolean d = true;
		char e = 127;
		
		System.out.println(a+b); 	// Int + int
		System.out.println(a+c);	// Int + String
		//System.out.println(a+d);
		System.out.println(a+e);	// Int + char --> Int
		System.out.println(b+c);	// double + String
		//System.out.println(b+d);
		System.out.println(b+e);	// double + char --> Double
		System.out.println(c+d);	// String + boolean --> Juliantrue
		System.out.println(c+e);	// String + char --> String
		System.out.println(a+a);	// int + int
		System.out.println(b+b);
		System.out.println(c+c);
		//System.out.println(d+d);
		System.out.println(e+e);
		
		
		e++;
		System.out.println(e);

	}

}
