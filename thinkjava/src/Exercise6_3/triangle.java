package Exercise6_3;

public class triangle {

	public static void main(String[] args) {
		double seiteA = 10;
		double seiteB = 15;
		double seiteC = 10;
		
		System.out.println((isTriangle(seiteA, seiteB, seiteC) == true)? "Dreieck m�glich" : "kein Dreieck m�glich");

	}
	
	public static boolean isTriangle(double seiteA, double seiteB, double seiteC) {
		if(seiteA+seiteB < seiteC) 
			return false;
		
		else if(seiteA+seiteC < seiteB) 
			return false;
		
		else if(seiteB+seiteC < seiteA) 
			return false;

		else 
			return true;
	}
}
