package Exercise8_6;

public class areFac {

	public static void main(String[] args) {
		int[] reihe = {0,5,10,15,20,30,40,45};
		int n = 5;
		
		System.out.println(areFactors(reihe, n));

	}
	
	public static boolean areFactors(int[] reihe, int n) {

		
		for(int element: reihe) {
			if(element % n != 0) {
				return false;
			}
		}
		
		return true;
	}

}
