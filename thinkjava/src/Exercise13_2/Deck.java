package Exercise13_2;

import java.util.Arrays;
import java.util.Random;

/**
 * A deck of playing cards (of fixed length).
 */
public class Deck {

	public static final Random rnd = new Random();
	private Card[] cards;

	/**
	 * Constructs a standard deck of 52 cards.
	 */
	public Deck() {
		this.cards = new Card[52];
		int index = 0;
		for (int suit = 0; suit <= 3; suit++) {
			for (int rank = 1; rank <= 13; rank++) {
				this.cards[index] = new Card(rank, suit);
				index++;
			}
		}
	}

	public static void main(String[] a) {
		Deck test = new Deck();
		
		test.shuffle();
		test.mergeSort();

		System.out.println(test.deckToString());


	}

	/**
	 * Constructs a deck of n cards (all null).
	 */
	public Deck(int n) {
		this.cards = new Card[n];
	}

	/**
	 * Gets the internal cards array.
	 */
	public Card[] getCards() {
		return this.cards;
	}

	/**
	 * Displays each of the cards in the deck.
	 */
	public void print() {
		for (Card card : this.cards) {
			System.out.println(card);
		}
	}

	/**
	 * Returns a string representation of the deck.
	 */
	public String toString() {
		return Arrays.toString(this.cards);
	}

	/**
	 * Randomly permutes the array of cards.
	 */
	public void shuffle() {
		for(int i = 0; i < cards.length; i++) {
			//int position;
			//do {
			int position = randomInt(i, cards.length-1);
			//} while(i == position);
			swapCards(i, position);
		}
	}

	/**
	 * Chooses a random number between low and high, including both.
	 */
	private static int randomInt(int low, int high) {
		return rnd.nextInt(high-low+1)+low;
	}

	/**
	 * Swaps the cards at indexes i and j.
	 */
	private void swapCards(int i, int j) {
		//		System.out.println("Swapping " + this.cards[i] + " and " + this.cards[j]);
		Card hold = this.cards[i];
		this.cards[i] = this.cards[j];
		this.cards[j] = hold;
	}

	/**
	 * Sorts the cards (in place) using selection sort.
	 */
	public void selectionSort() {
		for(int i = 0; i < cards.length-1; i++) {
			swapCards(i, indexLowest(i, cards.length-1));
		}
	}

	/**
	 * Finds the index of the lowest card
	 * between low and high inclusive.
	 */
	private int indexLowest(int low, int high) {
		int index = low;
		
		for(int i = low; i <= high; i++) {
			if(cards[i].compareTo(cards[index]) == -1) {
				index = i; 
			}
		}
	

		return index;
	}

	/**
	 * Returns a subset of the cards in the deck.
	 */
	public Deck subdeck(int low, int high) {
		Deck sub = new Deck(high - low + 1);
		for (int i = 0; i < sub.cards.length; i++) {
			sub.cards[i] = this.cards[low + i];
		}
		return sub;
	}

	/**
	 * Combines two previously sorted subdecks.
	 */

	private static Deck merge(Deck d1, Deck d2) {
		int necesarryLength = d1.cards.length + d2.cards.length;
		Deck mergedDeck = new Deck(necesarryLength);

		int indexFirstDeck = 0;
		int indexSecondDeck = 0;

		for(int i = 0; i < mergedDeck.cards.length; i++) { // Deck 1 leer? Karte von Deck 2 einsortieren!
			if(indexFirstDeck == d1.cards.length) {
				mergedDeck.cards[i] = d2.cards[indexSecondDeck];
				indexSecondDeck++;
			}
			else if(indexSecondDeck == d2.cards.length) {
				mergedDeck.cards[i] = d1.cards[indexFirstDeck];
				indexFirstDeck++; 
			}

			else {

				if(d1.cards[indexFirstDeck].compareTo(d2.cards[indexSecondDeck]) == -1) {
					mergedDeck.cards[i] = d1.cards[indexFirstDeck];
					indexFirstDeck++;
				}
				else if(d1.cards[indexFirstDeck].compareTo(d2.cards[indexSecondDeck]) == 1) {
					mergedDeck.cards[i] = d2.cards[indexSecondDeck];
					indexSecondDeck++;
				}
				else if(d1.cards[indexFirstDeck].compareTo(d2.cards[indexSecondDeck]) == 0) {
					mergedDeck.cards[i] = d1.cards[indexFirstDeck];
					i++;
					mergedDeck.cards[i] = d2.cards[indexSecondDeck];
					indexFirstDeck++;
					indexSecondDeck++;
				}
			}
		}

		return mergedDeck;
	}

	/**
	 * Returns a sorted copy of the deck using selection sort.
	 */
	public Deck almostMergeSort() {
		Deck subdeck1 = subdeck(0, (cards.length/2)-1);
		Deck subdeck2 = subdeck((cards.length/2), cards.length-1);
		
		subdeck1.selectionSort();
		subdeck2.selectionSort();
		
		return merge(subdeck1, subdeck2);
	
	}

	/**
	 * Returns a sorted copy of the deck using merge sort.
	 */
	public Deck mergeSort() {
		if(cards.length == 0 || cards.length == 1) {
			return this;
		}
		
		else {
		Deck subdeck1 = subdeck(0, (cards.length/2)-1);
		Deck subdeck2 = subdeck((cards.length/2), cards.length-1);
		
		subdeck1 = subdeck1.mergeSort();
		subdeck2 = subdeck2.mergeSort();
		
		return merge(subdeck1, subdeck2);
		}
		
	}

	/**
	 * Reorders the cards (in place) using insertion sort.
	 */
	public void insertionSort() {
		for(int i = 0; i < cards.length; i++) {
			Card valueToBeInserted = cards[i];
			int j = i;
			while(j >= 1 && cards[j-1].compareTo(valueToBeInserted) == 1) {
				swapCards(j-1, j);
				j = j-1;
			}
		}
		

	}
	public String deckToStrin1g() {
		StringBuilder sb = new StringBuilder();   
		for(int i = 0; i < cards.length; i++) {
			sb.append(cards[i] + ",\n");
		}
		return sb.toString();

	}
	
	public String deckToString() {
		String s = "";  
		for(int i = 0; i < cards.length; i++) {
			s += (cards[i] + ",\n");
		}
		return s;

	}
	

}
