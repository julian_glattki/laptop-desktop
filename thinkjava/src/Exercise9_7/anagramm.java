package Exercise9_7;

public class anagramm {
	String ab;
	public static void main(String[] args) {
		String s = "allo";
		String a = "olla";
		System.out.println(isAnagram(s, a));
		
		
		anagramm neu = new anagramm();
		neu.ab = "ab";
		System.out.println(neu);

	}

	public static boolean isAnagram(String erstesWort, String zweitesWort) {
		if(erstesWort.length() == zweitesWort.length()) {
			int[] erstesWortZähler = zählerInUppercase(erstesWort);
			int[] zweitesWortZähler = zählerInUppercase(zweitesWort);

			for(int i = 0; i <= 25; i++) {
				if(erstesWortZähler[i] != zweitesWortZähler[i]) {
					return false;
				}
			}

		}
		else {
			return false;
		}

		return true;

	}

	public static int[] zählerInUppercase(String zeichenkette) {
		zeichenkette = zeichenkette.toUpperCase();
		char[] charArray = zeichenkette.toCharArray();
		int[] zähler = new int[26];

		
		for(char element : charArray) {
			zähler[element-65]++;
		}

		return zähler;
	}

}
