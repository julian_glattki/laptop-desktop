package Exercise4_2;

/**
 * Exercise 4.2 The point of this exercise is to make sure you understand how
to write and invoke methods that take parameters.
1. Write the first line of a method named zool that takes three parameters:
an int and two Strings.
2. Write a line of code that calls zool, passing as arguments the value 11,
the name of your first pet, and the name of the street you grew up on.
 * @author julia
 *
 */
public class Zoo {

	public static void zoo1(int zahl, String zeichenkette, String zeichenkette2) {
		System.out.println(zahl + " " + zeichenkette + " " + zeichenkette2);
	}
	public static void main(String[] args) {
		zoo1(11, "Snoopy", "Fridtjof");

	}

}
