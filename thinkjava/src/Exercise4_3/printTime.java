package Exercise4_3;

public class printTime {
	
	public static void printAmerican(int day, String month, int year) {
		
		
		String tag = String.valueOf(day);
		
		if(day == 1) {
			tag += "st";
		}
		else if(day == 2) {
			tag += "nd";
		}
		else if (day == 3) {
			tag += "rd";
		}
		
		System.out.println(month + "/" + tag + "/" + year);
	}
	
	public static void printEuropean(int day, String month, int year) {
		System.out.println(day + "/" + month + "/" + year);
	}
	
	
	public static void main(String[] args) {
		int day = 3;
		String month = "November";
		int year = 2018;
		
		printAmerican(day, month, year);
		System.out.println();
		printEuropean(day, month, year);
	}

}
