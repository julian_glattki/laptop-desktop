package Exercise12_1;

public class Deck {

	public static Card[] createDeckWith52Cards() {
		Card[] deck = new Card[52];
		int index = 0;

		for(int suit = 0; suit < 4; suit++) {
			for(int rank = 1; rank < 14; rank++) {
				deck[index] = new Card(rank, suit);
				index++;
			}
		}

		return deck; 
	}

	public static void main(String[] args) {
		Card[] deck = createDeckWith52Cards();
		
		for(Card karte : deck) {
			karte.printCard();
		}
	}
}
