package Exercise10_4;

import java.math.BigInteger;

public class Big {

	public static void main(String[] args) {
		
		for(long i = 0; i <= 922337203685477585L; i++) {
			System.out.print(i + " --------");
			System.out.println(factorial(i));
		}

	}
	
	public static BigInteger factorial(long n) {
		long fakultät = 1;
		BigInteger big = BigInteger.valueOf(fakultät);
		
		for(int i = 1; i <= n; i++) {
			BigInteger big2 = BigInteger.valueOf(i);
			big = big.multiply(big2);
		}
		
		return big;
	}

}
