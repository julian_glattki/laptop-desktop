package Exercise3_2;

import java.util.Scanner;

public class CelsiusFahrenheit {

	public static void main(String[] args) {
		
		// Variablen
		double celsius;
		double fahrenheit;
		final double FORMEL_MULTIPLIER;
		final double FORMEL_ADD;
		
		// Liest Eingabe von Nutzer ein == EINGABE
		System.out.print("Geben Sie einen Wert in Grad-Celsius an, der zu Fahrenheit umgewandelt werden soll: ");
		Scanner in = new Scanner(System.in);
		celsius = in.nextDouble();
		in.close();
		
		// Umwandlung == VERARBEITUNG
		FORMEL_MULTIPLIER = (9.0/5.0);
		FORMEL_ADD = 32.0;
		fahrenheit = celsius * FORMEL_MULTIPLIER + FORMEL_ADD;
		
		// AUSGABE
		System.out.println("Fahrenheit: " + fahrenheit);
	}

}
