package Exercise9_5;

public class abredacbiraqsd {

	public static void main(String[] args) {
		String s = "?t";
		System.out.println(isAbecedarian(s));
		System.out.println(isAbecedarianMitForLoop(s));

	}

	public static boolean isAbecedarian(String wort) {
		int zähler = 0;
		int buchstabeAlsZahl = 0;
		wort = wort.toUpperCase();
		char[] charArray = wort.toCharArray();

		for(char element : charArray) {
			if(element >= buchstabeAlsZahl) {
				buchstabeAlsZahl = element;
				zähler++;
			}
		}

		if(zähler == wort.length()) {
			return true;
		}
		else {
			return false;
		}

	}
	public static boolean isAbecedarianMitForLoop(String wort) {
		wort = wort.toUpperCase();
		int block = 0;
		for(int i = 0; i < wort.length(); i++) {
			char c = wort.charAt(i);
			if(c >= 65 && c <= 90) {
				if(block <= c) {
					block = c;
				}
				else {
					return false;
				}
			}
			else {
				return false;
			}
		}
		return true;
	}
}
