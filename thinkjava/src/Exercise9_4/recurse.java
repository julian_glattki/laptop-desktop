package Exercise9_4;

public class recurse {

	public static void main(String[] args) {
		String s = "Hallo wie geht es dir";
		//		oneOnEachLine(s);
		//		printBackwards(s); 
//		System.out.println(recurse(s));
		
		String a = "lagerregal";
		System.out.println(isPalidrome(a));

	}

	public static boolean isPalidrome(String zeichenkette) {
		if(length(zeichenkette) == 0) {
			return false;
		}

		else if(length(zeichenkette) == 1) {
			return true;
		}

		else if(length(zeichenkette) == 2) {
			if(first(zeichenkette) == first(rest(zeichenkette))) {
				return true;
			}
			else {
				return false;
			}
		}

		else if(first(zeichenkette) == first(recurse(rest(zeichenkette)))) {
			return isPalidrome(middle(zeichenkette));
	}
	
		return false;
}




public static String recurse(String zeichenkette) {
	if(length(zeichenkette)!=0)
		return recurse(rest(zeichenkette)) + first(zeichenkette);

	return "";
}

public static void oneOnEachLine(String zeichenkette) {
	if(length(zeichenkette) != 0) {
		System.out.println(first(zeichenkette));
		oneOnEachLine(rest(zeichenkette));
	}

}

public static void printBackwards(String zeichenkette) {
	if(length(zeichenkette) != 0) {
		printBackwards(rest(zeichenkette));
		System.out.println(first(zeichenkette));
	}
}
/**
 * Returns the first character of the given String.
 */
public static char first(String s) {
	return s.charAt(0);
}
/**
 * Returns all but the first letter of the given String.
 */
public static String rest(String s) {
	return s.substring(1);
}

/**
 * Returns all but the first and last letter of the String.
 */
public static String middle(String s) {
	return s.substring(1, s.length() - 1);
}

/**
 * Returns the length of the given String.
 */
public static int length(String s) {
	return s.length();
}


}
