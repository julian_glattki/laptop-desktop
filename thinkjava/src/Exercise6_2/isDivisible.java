package Exercise6_2;

import java.util.Scanner;

public class isDivisible {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Zahl eingeben: ");
		int zahl = in.nextInt();
		System.out.print("Teiler eingeben: ");
		int teiler = in.nextInt();
		
		System.out.println(isDivisibleMethod(teiler, zahl));
		
		in.close();
	}
	public static boolean isDivisibleMethod(int teiler, int zahl) {
		int restDerDivision = zahl%teiler;
		if(restDerDivision == 0) {
			return true;
		}
		else {
			return false;
		}
	}
}
