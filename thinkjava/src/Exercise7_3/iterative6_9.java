package Exercise7_3;

public class iterative6_9 {

	public static void main(String[] args) {
		
		System.out.println(power(4,4));

	}
	
	public static double power(double x, int n) {
		double result = 1;

		for(int i = 0; i < n; i++) {
			result *= x;
		}
		
		return result; 
	}

}
