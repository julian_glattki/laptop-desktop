package Exercise9_6;

public class doubloon {

	public static void main(String[] args) {
		String s = "hheelloo";
		
		System.out.println(isDoubloon(s));

	}
	
	public static boolean isDoubloon(String wort) {
		wort = wort.toUpperCase();
		char[] charArray = wort.toCharArray();
		int[] alphabetArray = new int[26];
		
		
		for(char element : charArray) {
			if(element < 65 || element > 90)
				return false;
		}
		for(int i = 0; i < wort.length(); i++) {
			char c = charArray[i];
			alphabetArray[c-65]++;	
		}
		
		for(int element : alphabetArray) {
			if(element == 0 || element == 2) {
				continue;
			}
			
			else {
				return false;
			}
		}
		
		return true;
	}

}
