package projectEuler;

public class task5 {

	public static void main(String[] args) {
		System.out.println(smallestMultiple(20));

	}
	public static boolean isDivisibleForAll(int number, int largestDivisor) {
		for(int i = 1; i <= largestDivisor; i++) {
			if(number % i != 0) {
				return false;
			}
		}
		return true;
	}
	public static int smallestMultiple(int largestDivisor) {
		int max = 0; 
		for(int i = 1; i < Integer.MAX_VALUE; i++) {
			if(isDivisibleForAll(i, largestDivisor)) {
				max = i;
				break;
			}
		}
		return max;
	}
}


