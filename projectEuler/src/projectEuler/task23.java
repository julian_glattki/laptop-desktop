package projectEuler;

import java.util.ArrayList;
// Gute L�sung
public class task23 {
	
	
	public static void main(String[] args) {
		
		final int MAX= 28124;
		boolean abSum[] = new boolean[MAX];
		int divSum, sum = 0;
		
		for(int i = 0; i < abSum.length; i++) {
			divSum = 0; 
			sum += i;
			
			for(int e = 1; e < i; e++) {
				if(i % e == 0) {
					divSum += e;
				}
			}
			
			if(divSum > i) {
				abSum[i] = true;
			}
		}
		
		for(int i = 1; i < abSum.length; i++) {
			for(int e = 1; e <= i; e++) {
				if(abSum[e] && abSum[i-e]) {
					sum -= i;
					break;
				}
			}
		}
		
		System.out.println(sum);
	}
	
}