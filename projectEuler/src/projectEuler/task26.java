package projectEuler;

import java.math.BigDecimal;

// beste L�sung mit HASH-MAPS, andere zu kompliziert
public class task26 {
	
	public static final double MAXIMUM = 999;
	public static void main(String[] args) {
		findLongestReciprocalCycle();

	}
	
	public static double findLongestReciprocalCycle() {
		for(double i = 1.0; i <= MAXIMUM; i++) {
			BigDecimal value = BigDecimal.valueOf(1.0/i);
			String valueString = value.toString();
			System.out.println(valueString);
			
		}
		
		
		return 0.0;
	}

}
