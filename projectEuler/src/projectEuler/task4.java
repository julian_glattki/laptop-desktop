package projectEuler;

public class task4 {

	public static void main(String[] args) {
		System.out.println(findLargestPalindrome(999));

	}

	public static int findLargestPalindrome(int max) {
		int largest = 0; 
		int big = 0; 

		for(int i = 0; i <= max; i++) {
			for(int j = 0; j <= max; j++) {
				if(isPalindrome1(j*i)) {
					largest = j*i;
					if(largest > big) {
						big = largest;
					}
					
				}
			}
		}

		return big;
	}
	public static boolean isPalindrome(int number) {
		String reversed = "";
		String ordinaryOrder = String.valueOf(number);
		char[] ordinaryOrderChar = ordinaryOrder.toCharArray();

		for(int i = ordinaryOrderChar.length-1; i >= 0; i--) {
			reversed += ordinaryOrderChar[i];
		}

		if(ordinaryOrder.equals(reversed)) {
			return true;
		}

		return false;

	}
	
	public static boolean isPalindrome1(int number) {
		char[] numberChar = String.valueOf(number).toCharArray();
		
		for(int i = 0; i < (numberChar.length/2); i++) {
			if(numberChar[i] != numberChar[numberChar.length-i-1]) {
				return false;
			}
		}
		return true;
	}
}
