package projectEuler;

public class task6 {
	public static void main(String[] args) {
		System.out.println(difference(sumOfSquares(1,100), squareOfSum(1,100)));
	}
	
	public static int sumOfSquares(int start, int end) {
		int sum = 0; 
		for(int i = start; i <= end; i++) {
			sum += Math.pow(i,2);
		}
		
		return sum;
	}
	
	public static int squareOfSum(int start, int end) {
		int sum = 0; 
		
		for(int i = start; i <= end; i++) {
			sum += i;
		}
		
		return (int)Math.pow(sum, 2);
 	}
	
	public static int difference(int number1, int number2) {
		int test = Integer.compare(number1, number2);
		
		if(test > 0) {
			return number1-number2;
		}
		else {
			return number2-number1;
		}
	}
}
