package projectEuler;

import java.math.BigInteger;

public class task20 {

	public static void main(String[] args) {
			System.out.println(digitSum(bigFactorial(100)));

	}
	
	public static int digitSum(BigInteger number) {
		String numberAsString = number.toString();
		System.out.println(numberAsString);
		int sum = 0;
		for(int i = 0; i < numberAsString.length(); i++) {
			sum += Character.getNumericValue(numberAsString.charAt(i));
		}
		
		return sum; 
	}
	
	public static BigInteger bigFactorial(int factorial) {
		BigInteger bigFactorial = BigInteger.ONE;
		
		long iterator = (long) factorial;
		
		for(Long i = 2L; i <= iterator; i++) {
			bigFactorial = bigFactorial.multiply(BigInteger.valueOf(i));
		}
		
		return bigFactorial;
	}
}
