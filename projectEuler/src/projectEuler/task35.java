package projectEuler;

import java.util.ArrayList;

public class task35 {
	public static void main(String[] args ) {


		ArrayList<Integer> al = new ArrayList<Integer>();
		
		for(int i = 2; i < 1000000; i++) {
			if(isPrime(i)) {
				int test = 1; 
				int circular = 0; 
				for(int j = 0; j < String.valueOf(i).length(); j++) {
					if(j == 0) {
						circular = getNextCircular(i);
					}
					else {
						circular = getNextCircular(circular);
					}

					if(!(isPrime(circular))) {
						break;
					}
					test++;
				}

				if(test-1 == String.valueOf(i).length()) {
					System.out.println(i);
					al.add(i);
				}
			}
		}

		System.out.println(al.size());
	}
	public static boolean isPrime(int n) {
		int counter = 0; 

		for(int i = 2; i <= n/2; ++i) {
			if(n%i == 0) {
				return false;
			}
		}

		return true;
	}

	public static int getNextCircular(int n) {
		String number = String.valueOf(n);

		String circularNumber = ""; 
		circularNumber += Character.getNumericValue(number.charAt(number.length()-1));

		for(int i = 0; i <= number.length() -2; i++) 	{
			circularNumber += Character.getNumericValue(number.charAt(i));
		}
		return Integer.parseInt(circularNumber);		
	}


}
