package projectEuler;

public class task27 {
	private static final int MAX_A = 1000;
	private static final int MAX_B = 1000;
	
	public static void main(String[] args) {
		int max = 0; 
		int saveA = 0;
		int saveB = 0;
		
		for(int a = 0; a < MAX_A; a++) {
			for(int b = 0; b < MAX_B; b++) {
				int quadPrimes = quadraticPrimes(a,b);
				if(quadPrimes > max) {
					max = quadPrimes;
					saveA = a;
					saveB = b;
				}
			}
		}
		
		System.out.println(saveA*saveB);

	}
	
	
	
	public static boolean isPrime(double number) {
		if(number % 1 != 0) {
			return false;
		}
		
		int counter = 0;
		
		for(int i = 1; i <= number; i++) {
			if(number % i == 0) {
				counter++;
			}
		}
		
		if(counter == 2) {
			return true;
		}
		
		return false;
	}

	public static int quadraticPrimes(double a, double b) {
		int n = 0;
		int counter = 0;
		
		double quadraticFormula = Math.pow(n,2) - a*n + b;
		
		while(isPrime(quadraticFormula)) {
			n++;
			counter++;
			quadraticFormula = Math.pow(n,2) - a*n + b;
		}
		
		return counter;
	}
}
