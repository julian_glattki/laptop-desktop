package projectEuler;

import java.util.ArrayList;

public class task2 {
	public static void main(String[] args) {
		System.out.println(sumOfEven(fibonacci(4000000)));
	}

	public static int sumOfEven(ArrayList<Integer> numbers) {
		int sum = 0;
		
		for(int i : numbers) {
			if(i % 2 == 0) {
				sum += i;
			}
		}
		
		return sum;
	}
	public static ArrayList<Integer> fibonacci(int max) {
		int start = 0; 
		int mid = 1; 
		int fibonacci = 0; 
		ArrayList<Integer> fibonacciNumbers = new ArrayList<Integer>();
		
		while(fibonacci <= max) {
			fibonacci = start + mid;
			
			fibonacciNumbers.add(fibonacci);
			
			start = mid; 
			mid = fibonacci;
			
			
		}
		
		return fibonacciNumbers;
	}
}