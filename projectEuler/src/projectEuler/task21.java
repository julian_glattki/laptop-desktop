package projectEuler;

public class task21 {

	public static void main(String[] args) {
		System.out.println(sumOfAmicableNumbers(10000));
	}
	
	public static int sumOfAmicableNumbers(int max) {
		int sum = 0; 
		
		for(int i = 0; i < max; i++) {
			if(isAmicable(i)) {
				sum += i;
			}
		}
		
		return sum;
	}
	public static int getSumOfProperDivisors(int number) {
		int sum = 0;

		for(int i = 1; i < number; i++) {
			if(number % i == 0) {
				sum += i; 
			}
		}
		return sum;
	}
	
	public static boolean isAmicable(int number) {
		int m = getSumOfProperDivisors(number);
		return number == getSumOfProperDivisors(m) && m!=number;
	}

}
