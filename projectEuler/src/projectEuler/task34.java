package projectEuler;

public class task34 {

	public static void main(String[] args) {
		int sumOfFactorialDigits = 0; 
		int sum = 0; 
		for(int i = 10; i < 1000000; i++) {
			for(int j = 0; j < String.valueOf(i).length(); j++) {
				sumOfFactorialDigits += factorial(Character.getNumericValue(String.valueOf(i).charAt(j)));
			}
			
			if(sumOfFactorialDigits == i) {
				sum += i;
			}
			sumOfFactorialDigits = 0; 
		}
		System.out.println(sum);
	}

	public static int factorial(int n) {
		int factorial = 1;
		for(int i = 1; i <= n; i++) {
			factorial *= i;
		}
		
		return factorial;
	}
}
