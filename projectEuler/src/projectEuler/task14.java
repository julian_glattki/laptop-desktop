package projectEuler;

public class task14 {
	public static int getNextCollatzNumber(int number) {
		if(number % 2 == 0) {
			return number/2;
		}
		else if(number % 2 != 0) {
			return (number*3)+1;
		}
		
		else {
			return -1;
		}
	}
	
	public static int findLongestChainStartingNumber(int max) {
		int counter = 0;
		int collatzNumber = 0; 
		int maxCounter = 0; 
		int longestStartingAChain = 0; 
		
		for(int i = 1; i < max; i++) {
			int startingNumber = i; 
			collatzNumber = i;
			while(collatzNumber != 1) {
				counter++;
				collatzNumber = getNextCollatzNumber(collatzNumber);
			}
			
			if(counter > maxCounter) {
				maxCounter = counter;
				longestStartingAChain = startingNumber;
			}
		}
		
		return longestStartingAChain;
	}
	
	public static void main(String[] args) {
		System.out.println(findLongestChainStartingNumber(1000000));
	}
}


