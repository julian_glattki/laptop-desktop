package projectEuler;

public class task37 {

	public static void main(String[] args) {
		int sum = 0; 
		for(int i = 10; i < 1000000; i++) {
			if(isPrime(i)) {
				boolean valid = true;
				String prime = Integer.toString(i);
				int length = prime.length();

				for(int j = 1; j < length; j++) {
					if(!(isPrime(Integer.parseInt(prime.substring(j))))) {
						valid = false;
					}
					if(!(isPrime(Integer.parseInt(prime.substring(0, length -j))))) {
						valid = false;
					}
				}
				if(valid) {
					System.out.println("Calculating....");
					sum+=i;
				}
			}
		}
		System.out.println(sum);

	}


	public static boolean isPrime(int n) {
		boolean prime = true;
		
		if(n < 2) {
			prime = false;
		}
		for(int i = 2; i <= n/2; ++i) {
			if(n%i == 0) {
				prime = false;
				break;
			}
		}
		
		return prime;
		
	}


}
