package projectEuler;

public class task1 {

	public static void main(String[] args) {
		System.out.println(sumOfMultiples(3,5,999));

	}
	
	public static int sumOfMultiples(int multiple1, int multiple2, int max) {
		int sum = 0; 
		
		for(int i = 0; i <= max; i++) {
			if(i % multiple1 == 0 || i % multiple2 == 0) {
				sum += i; 
			}
		}
		
		return sum;
	}
}
