package projectEuler;

import java.math.BigInteger;

public class task15 {

	public static void main(String[] args) {
		
		System.out.println(binomialCoefficent(40L, 20L).toString());

	}
	public static BigInteger binomialCoefficent(Long n, Long k) {
		return factorial(n).divide(factorial(k).multiply(factorial(n-k)));
	}
	public static BigInteger factorial(Long max) {
		BigInteger factorial = BigInteger.ONE;
		for(Long i = 2L; i <= max; i++) {
			factorial = factorial.multiply(BigInteger.valueOf(i));
		}
		
		return factorial;
	}

}
