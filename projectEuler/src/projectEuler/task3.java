package projectEuler;

import java.util.ArrayList;

public class task3 {

	public static void main(String[] args) {
//		for(long prime : getPrimes(10000L)) {
//			System.out.println(prime);
//		}
		
		System.out.println(largestPrimeFactor(600851475143L, getPrimes(10000L)));
	}
	public static ArrayList<Long> getPrimes(long max) {
		int divisor = 0; 
		ArrayList<Long> primes = new ArrayList<Long>();

		for(long i = 2L; i <= max; i++ ) {
			for(long j = 1L; j <= i; j++) {
				if(i % j == 0) {
					divisor++;
				}
			}

			if(divisor == 2) {
				primes.add(i);
				divisor = 0;
			}

			else {
				divisor = 0;
			}
		}
		;
		return primes; 
	}
	
	public static long largestPrimeFactor(long number, ArrayList<Long> primes) {
		Long max = 0L;

		for(Long divide : primes) {
			if(number % divide == 0L) {
				max = divide;
			}
		}

		return max;
	}

}
