package projectEuler;

public class task30 {

	public static void main(String[] args) {
		int sum = 0; 
		
		for(int i = 10; i <= 200000; i++){
			String[] numbers = String.valueOf(i).split("");
			int sumOfPoweredDigits = 0;
			
			for(int j = 0; j < numbers.length; j++) {
				sumOfPoweredDigits += Math.pow(Double.parseDouble(numbers[j]), 5);
			}
			
			if(sumOfPoweredDigits == i) {
				sum += i;
			}
			
		}
		
		System.out.println(sum);

	}

}
