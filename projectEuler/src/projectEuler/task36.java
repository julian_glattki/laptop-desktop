package projectEuler;

public class task36 {

	public static void main(String[] args) {

		int sum = 0; 

		for(int i = 1; i < 1000000; i++) {
			String numberAsBinary = Integer.toBinaryString(i);
			String numberAsDecimal = String.valueOf(i);

			if(isPalindrome(numberAsBinary) && isPalindrome(numberAsDecimal)) {
				sum += i; 
			}
		}
		
		System.out.println(sum);
	}

	public static boolean isPalindrome(String number) {
		String reversed = ""; 

		for(int i = number.length()-1; i >= 0; i--) {
			reversed += number.charAt(i);
		}

		for(int i = 0; i < number.length(); i++) {
			if(number.charAt(i) != reversed.charAt(i)) {
				return false;
			}
		}

		return true;
	}
}

