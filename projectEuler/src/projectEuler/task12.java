package projectEuler;

public class task12 {
	public static void main(String[] args) {
		System.out.println(getFirstTriangular(500));
	}
	
	public static int getFirstTriangular(int noOfDivisors) {
		boolean found = false;
		int i = 0;
		int triangularNumber = 0; 
		
		while(!found) {
			i++;
			triangularNumber += i;
			if(countDivisor(triangularNumber) > 500) {
				found = true;
			}
		}
		
		return triangularNumber;
	}
	public static int countDivisor(int number) {
		int counter = 0; 
		
		for(int i = 1; i <= number; i++) {
			if(number % i == 0) {
				counter++;
			}
		}
		
		return counter;
	}
}
