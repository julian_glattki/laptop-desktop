package projectEuler;

import java.math.BigInteger;


public class task10 {

	public static void main(String[] args) {
		
		boolean[] primes = sieveOf(2000000);
		BigInteger sum = BigInteger.ZERO;
		

		for(int i = 2; i < primes.length; i++) {
			if(primes[i] == true) {
				sum = sum.add(BigInteger.valueOf(i));
			}
		}
		
		System.out.println(sum);

	}


	public static boolean[] sieveOf(int noOfPrimes) {
		boolean prime[] = new boolean[noOfPrimes+1];

		for(int i = 0; i < prime.length; i++) {
			prime[i] = true;
		}

		for(int j = 2; j*j <= noOfPrimes; j++) {
			if(prime[j] == true) {
				for(int k = j*j; k <= noOfPrimes; k+=j) {
					prime[k] = false;
				}
			}

		}
		return prime;
	}
}


