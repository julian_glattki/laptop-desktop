package projectEuler;

import java.math.BigInteger;

public class task16 {

	public static void main(String[] args) {
		System.out.println(sumOfDigits(power(2L, 1000)));

	}
	public static int sumOfDigits(BigInteger number) {
		String numbers = number.toString();
		int sum = 0; 
		for(int i = 0; i < numbers.length(); i++) {
			sum += Character.getNumericValue(numbers.charAt(i));
		}
		
		return sum; 
	} 
	public static BigInteger power(Long base, int exponent) {
		BigInteger powered = (BigInteger.valueOf(base)).pow(exponent);
		return powered;
	}

}
