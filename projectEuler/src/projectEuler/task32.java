package projectEuler;

import java.util.ArrayList;

public class task32 {

	public static void main(String[] args) {
		System.out.println(findUnusualProducts());

	}
	public static int findUnusualProducts() {
		int sum = 0; 
		ArrayList<Integer> al = new ArrayList<Integer>();
		
		for(int i = 0; i < 10000; i++) {
			for(int j = 0; j < 1000; j++) {
				String contancated = j + "" + i + "" + (j*i);
				if(isPandigital(9,contancated)) {
					if(!al.contains(j*i)) {
					sum += j*i;
					al.add(j*i);
					}
				}
			}
		}
		
		return sum;
	}
	public static boolean isPandigital(int oneToNumber, String numbers) {
		int[] check = new int[oneToNumber+1];
		
		for(int i = 0; i < numbers.length(); i++) {
			if(Character.getNumericValue(numbers.charAt(i)) == 0) {
				return false;
			}
			if(Character.getNumericValue(numbers.charAt(i)) < check.length) {
			check[Character.getNumericValue(numbers.charAt(i))]++;
			}
		}
		for(int i = 1; i < check.length; i++) {
			if(check[i] != 1) {
				return false;
			}
		}
		
		return true;
	}
}
