package projectEuler;

import java.util.Random;

public class task9 {

	public static void main(String[] args) {
		System.out.println(betterApproach(1000));
//		System.out.println(productOfPythagoras(1000));

	}
	
	public static int betterApproach(int sum) {
		for(int i = 1; i < sum; i++) {
			for(int j = i+1; j < sum; j++) {
				int c = sum-i-j;
				
				if(c*c == i*i + j*j) {
					return i*j*c;
				}
			}
		}
		return -1;
	}
	public static double productOfPythagoras(int sum) {
		Random rnd = new Random();
		boolean found = true;
		double a = 0; 
		double b = 0; 
		double c = 0;
		
		while(found) {
			a = ((double) rnd.nextInt(sum));
			b = ((double) rnd.nextInt(sum));
			c = ((double) rnd.nextInt(sum));
			
			if(a*a + b*b == c*c) {
				if(c == sum - Math.sqrt(a) + Math.sqrt(b)) {
				found = false;
				}
			}
			

		}
		
		return a*b*c;
	}
}
