package projectEuler;

import java.util.ArrayList;

public class task7 {

	public static void main(String[] args) {
		ArrayList<Integer> primes = getPrimes(10001);
		System.out.println(primes.get(primes.size()-1));

		
	}
	public static ArrayList<Integer> getPrimes(int amount) {
		ArrayList<Integer> primes = new ArrayList<Integer>();
		int counter = 0;
		int max = 1;
		while(primes.size() < amount) {
			max++;
			for(int j = 1; j <= max; j++) {
				if(max%j == 0) {
					counter++;
				}
			}
			if(counter == 2) {
				counter = 0;
				primes.add(max);
			}
			else {
				counter = 0;
			}
		}
	return primes;
}
}
