package Warenwirtschaftssystem;

public abstract class Produkt {
	protected int artikelnummer;
	protected String name;
	protected double preis;
	
	public Produkt(int artikelnummer, String name, double preis) {
		this.artikelnummer = artikelnummer;
		this.name = name;
		this.preis = preis;
	}
	
	public abstract String getProdukt();
}

	
