package Warenwirtschaftssystem;

public class Hygieneprodukt extends Produkt{
	private String ablaufdatum;
	
	public Hygieneprodukt(int artikelnummer, String name, double preis, String ablaufdatum) {
		super(artikelnummer, name, preis);
		this.ablaufdatum = ablaufdatum;
		
	}


	public String getProdukt() {
		return this.name + "|" + this.artikelnummer + "|" + this.preis +"|" + this.ablaufdatum;
	}

}
