package SichereKlausuraufgabeN;

public class Kniffel {

	public static void main(String[] args) {
		int[] test = {4,3,2,1,5};

		System.out.println(isBigStreet(test));

	}

	public static boolean isBigStreet(int[] thrown) {
		boolean isBigStreet = false;
		int counter = 0; 

		boolean[] thrownB = new boolean[6];

		for(int i = 0; i < thrown.length; i++) {
			thrownB[thrown[i]] = true;
		}
		for(int j = 0; j <= 1; j++) {
			for(int i = j; i < thrownB.length - ((j==0)? 1 : 0); i++) {
				if(thrownB[i] == true) {
					counter++;
				}
			}
			if(counter == 5) {
				isBigStreet = true;
				break;
			}
			counter = 0;
		}

		return isBigStreet;
	}
}
