package SichereKlausuraufgabeN;

public class anzahlGroesser {

	public static void main(String[] args) {
		int[][] testArray = new int[2][3];
		for(int i = 0; i < testArray.length; i++) {
			for(int j = 0; j < testArray[0].length; j++) {
				testArray[i][j] = (int) (Math.random() * 15);
				System.out.print(testArray[i][j] + "|");
			}
		}
		System.out.println();
		
		System.out.println(getAnzahlGroesserAls(testArray, 5));
	}
	
	public static int getAnzahlGroesserAls(int[][] n, int test) {
		int counter = 0; 
		int i = 0; 
		int j = 0; 
		
		
		while(n.length > i) {
			while(n[i].length > j) {
				if(n[i][j] > test) {
					counter++;
				}
				j++;
			}
			i++;
			j = 0;
		}
		
		return counter;
	}

}
