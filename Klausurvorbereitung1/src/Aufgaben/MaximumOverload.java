package Aufgaben;

public class MaximumOverload {

	public static void main(String[] args) {
		int[] numbers = {1,2,3,4,5,6,7,8,10,25,2,3,4};
		
		System.out.println(max(numbers));
		System.out.println(max(1,2));
		System.out.println(max(1,2,3,4,5));
		System.out.println(max(1,2,3));

	}
	
//	public static int max(int... n) {
//		int max = 0; 
//		int maxOfTwo = 0; 
//		
//		for(int i = 0; i < n.length-1; i++) {
//			for(int j = 1; j < n.length; j++) {
//			maxOfTwo = Math.max(n[i], n[j]);
//			if(maxOfTwo > max) {
//				max = maxOfTwo;
//				maxOfTwo = 0;
//			}
//			}
//		}
//		
//		return max;
//	}
	
	
	
	public static int max(int... n) {
		int max = 0; 
		
		for(int element : n) {
			if(element > max) {
				max = element;
			}
		}
		
		return max;
	}
//	public static int max(int number1, int number2, int number3) {
//		return Math.max(number3, Math.max(number1, number2));
//	}
//
//	public static int max(int number1, int number2) {
//		return Math.max(number1, number2);
//	}
//
//	public static int max(int number1, int number2, int number3, int number4, int number5) {
//		int[] numbers = {number1, number2, number3, number4, number5};
//		
//		return max(numbers);
//	}
	
//	public static int max(int[] numbers) {
//		int max = 0; 
//		
//		for(int i = 0; i < numbers.length; i++) {
//			if(numbers[i] > max) {
//				max = numbers[i];
//			}
//		}
//		
//		return max;
//	}
//}
}