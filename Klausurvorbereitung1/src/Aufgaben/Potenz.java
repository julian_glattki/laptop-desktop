package Aufgaben;

public class Potenz {

	public static void main(String[] args) {
		int base = 4;
		int exponent = 5;
		
		System.out.println(potenzMitWhile(base, exponent));
		System.out.println(potenzMitDoWhile(base, exponent));
		System.out.println(potenzMitZaehlschleife(base, exponent));
		System.out.println(potenzMitRekursion(base, exponent));

	}
	
	public static int potenzMitWhile(int base, int exponent){
		int result = 1; 
		
		if(exponent < 0) {
			return -1;
		}
		while(exponent != 0) {
			result *= base;
			exponent--;
			}
		
		return result;
		
	}
	public static int potenzMitDoWhile(int base, int exponent){
		int result = 1; 
		
		if(exponent < 0) {
			return -1;
		}
		
		do {
			result *= base; 
			exponent--;
		}while(exponent != 0);
		
		return result;
	}
	public static int potenzMitZaehlschleife(int base, int exponent){
		int result = 1;
		
		if(exponent < 0) {
			return -1;
		}
		
		for(int i = 0; i < exponent; i++) {
			result *= base;
		}
		
		return result;
	}
	public static int potenzMitRekursion(int base, int exponent){
		if(exponent < 0) {
			return -1;
		}
		
		if(exponent == 1) {
			return base;
		}
		
		else {
			return base*potenzMitRekursion(base, exponent-1);
		}
		
		
	}

}
