package RekursiveAufgaben;

public class fibonacciRekusriv {

	public static void main(String[] args) {
		System.out.println(fibRekursiv(50));

	}
	
	public static int fibRekursiv(int n) {
		int fib = 1;
		if(n == 0) {
			return 0;
		}
		if(n == 1) {
			return 1;
		}
		
		else {
			return fibRekursiv(n-1) + fibRekursiv(n-2);
		}
		
		
	}

}
