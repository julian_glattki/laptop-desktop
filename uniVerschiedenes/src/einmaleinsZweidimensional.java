
public class einmaleinsZweidimensional {

	public static void main(String[] args) {
		int [][] einmaleins = einmaleins(10);
		
		for(int i = 0; i < 10 ; i++) {
			for(int j = 0; j < 10; j++) {
				System.out.print(i+1);
				System.out.print("*");
				System.out.print(j+1);
				System.out.println("=" + einmaleins[i][j]);
			}
		}

	}
	public static int[][] einmaleins(int max) {
		int[][] einmaleins = new int[max*max][max*max];
		
		for(int i = 0; i <= max; i++) {
			for(int j = 0; j <= max; j++) {
				einmaleins[i][j] = (j+1) * (i+1);
			}
		}
		
		return einmaleins;
	}
}
