package dartPi;

import java.util.ArrayList;

public class Dart {
	private double x; 
	private double y; 
	
	private static double radius = 1;
	private static double attempts = 40000000;
	
	public Dart() {
		this.x = Math.random(); 
		this.y = Math.random();
	}
	public static void main(String[] args) {
		double sum = 0; 
		
		for(int i = 0; i < 100; i++) {
		ArrayList<Dart> thrownDarts = createDarts(attempts);
		double inside = dartsInsideCircle(thrownDarts);
		double pi = calculatePi(inside);
		sum += pi;
		}
//		System.out.println(calculatePi(inside));
		System.out.println(sum/100);
		
		
	}
	
	public static double calculatePi(double dartsInsideCircle) {
		double ratio = dartsInsideCircle/attempts;
		double pi = ratio*4;
		return pi;
	}
	public static double dartsInsideCircle(ArrayList<Dart> thrownDarts) {
		double dartsInsideCircle = 0;; 
		
		double distanceMiddleToPoint; 
		
		for(Dart element : thrownDarts) {
			distanceMiddleToPoint = Math.sqrt(Math.pow((element.x), 2) + Math.pow(element.y, 2));
			if(distanceMiddleToPoint < radius) {
				dartsInsideCircle++;
			}
		}
		
		return dartsInsideCircle;
	}
	public static ArrayList<Dart> createDarts(double amount) {
		ArrayList<Dart> thrownDarts = new ArrayList<Dart>();
		
		for(int i = 0; i < amount; i++) {
			Dart thrownDart = new Dart();
			thrownDarts.add(thrownDart);
		}
		
		return thrownDarts;
	}
	
	

}
