package dartPi;

public class prozedural {

	public static void main(String[] args) {
		System.out.println(calculatePi(Integer.MAX_VALUE));

	}
	public static double calculatePi(int darts) {
		
		double hits = 0;
		
		for(int i = 0; i < darts; i++) {
			double x = Math.random();
			double y = Math.random();

			double distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
			
			if(distance <= 1) {
				hits++;
			}
		}
		
		double pi = hits/darts;
		
		return pi*4;
	}
}
