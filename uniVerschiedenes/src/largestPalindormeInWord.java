import java.util.ArrayList;

public class largestPalindormeInWord {

	public static void main(String[] args) {
		System.out.println(largestPalindrome("HalloKamelLagerregalAnnaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHalloKabemlafhasiufhhduafhiudfhuidsfiudsPLagerregalLagerregalolizei"));

	}

	public static boolean isPalindrome(ArrayList<Character> al) {
		char[] test = new char[al.size()];
		int index = 0;
		String reversed = ""; 
		String normal = "";
		for(int i = 0; i < al.size(); i++) {
			test[i] = al.get(i);
			normal += al.get(i);
		}

		for(int j = test.length-1; j >= 0; j--) {
			reversed += test[j];
		}

		if(normal.equals(reversed)) {
			return true;
		}

		return false;
	}
	public static String largestPalindrome(String word) {
		String test = ""; 
		int max = 0; 
		char[] wordCharArray = word.toUpperCase().toCharArray();

		ArrayList<Character> testArrayList = new ArrayList<Character>();


		for(int i = 0; i < word.length(); i++) {
			testArrayList.add(wordCharArray[i]);
			for(int j = 1+i; j < word.length(); j++) {
				testArrayList.add(wordCharArray[j]);
				if(isPalindrome(testArrayList)) {
					if(testArrayList.size() >= max) {
						max = testArrayList.size();
						test = "";
						for(char letter : testArrayList) {
							test += letter;
						}
					}
				}
			}
			testArrayList.clear();
		}



		return test;
	}
}
