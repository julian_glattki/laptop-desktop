
public class aufgabe6TestatBeispiel {
	
	public static void main(String[] args) {
		int[] test = {25,5,5,5,5,10};
		System.out.println(zieheAbVonErster(test));
	}

	public static int zieheAbVonErster(int[] arr) {
		int length = arr.length -1;
		
		while(length > 0) {
			arr[0] -= arr[length];
			length--;
		}
		
		return arr[0];
	}
}
