import java.math.BigInteger;

public class IBAN {
	static BigInteger MODULO_WERT = BigInteger.valueOf(97L);
	public static void main(String[] args) {
		String test = "DE02120300000000202051";
		
		System.out.println(isValid("35785783", "6767798396", "131400"));

	}


	public static String isValid(String bankleihzahl, String kontonummer, String laendercode) {
		String testString = bankleihzahl + "" + kontonummer + "" + laendercode;
		BigInteger allParts = new BigInteger(testString);
		
		BigInteger rest = allParts.mod(MODULO_WERT);
		
		int pruefzifferInteger = 98 - rest.intValue();
		String pruefziffer = Integer.toString(pruefzifferInteger);
		
		if(pruefziffer.length() < 2) {
			String hold = pruefziffer;
			pruefziffer = "0"+ hold; 
			
		}
		
		return pruefziffer;
	}
}
