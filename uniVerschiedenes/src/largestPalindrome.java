
public class largestPalindrome {

	public static void main(String[] args) {
		String given = "HalloKamelAnnaAAAAABXXASAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHalloKabemlafhasiufhhduafhiudfhuidsfiudsPLagerregalLagerregalolizei";
		
		System.out.println(findLargestPalindrome(given));
	}
	
	public static boolean isPalindrome(String substring) {
		substring = substring.toUpperCase();
		String reversed = ""; 
		
		for(int i = substring.length()-1; i >= 0; i--) {
			reversed += substring.charAt(i);
		}
		
		if(reversed.equals(substring)) {
			return true;
		}
		
		return false;
	}
	public static String findLargestPalindrome(String given) {
		given = given.toUpperCase();
		String substring = ""; 
		String max = ""; 
		int start = 1; 
		
		for(int i = 0; i < given.length(); i++) {
			substring += given.charAt(i);
			for(int j = start; j < given.length(); j++) {
				substring += given.charAt(j);
				
				if(isPalindrome(substring)) {
					if(substring.length() > max.length()) {
						max = substring;
					}
				}
			}
			start++;
			substring = ""; 
		}
		return max;
	}
}
