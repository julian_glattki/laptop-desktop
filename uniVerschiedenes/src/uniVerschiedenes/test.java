package uniVerschiedenes;

import java.awt.*;

public class test {

	public static void main(String[] args) {


		   Rectangle box1 = new Rectangle(2,4,7,9);
		   Point p1 = findCenter(box1);
		   printPoint(p1);
		   System.out.println(box1);
		   box1.grow(1, 1);
		   System.out.println(box1);
		   Point p2 = findCenter(box1);
		   System.out.println(p2);
		   printPoint(p2);
	}

	public static double distance(Point p1, Point p2){
		int dx = p2.x - p1.x;
		//System.out.println(dx);
		int dy = p2.y - p1.y;
		//System.out.println(dy);
		return Math.sqrt(dx*dx+dy*dy);
	}

	public static Point findCenter(Rectangle box){
		int x = box.x + box.width / 2;
		System.out.println(x);
		int y = box.y + box.height / 2;
		System.out.println(y);
		return new Point(x, y);
	}
	   public static void printPoint(Point p){
		      System.out.println("("+p.x+", "+p.y+")");
		   }

}