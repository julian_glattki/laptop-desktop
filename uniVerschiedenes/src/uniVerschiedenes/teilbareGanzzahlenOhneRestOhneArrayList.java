package uniVerschiedenes;

public class teilbareGanzzahlenOhneRestOhneArrayList {

	public static void main(String[] args) {

		for(int element: teilbareGanzzahlen(3,23,3))
			System.out.println(element);
	}


	public static int[] teilbareGanzzahlen(int startwert, int endwert, int n) {
		int index = 0;
		int länge = 0;
		
		for(int i = startwert; i <= endwert; i++) {
			if(i % n == 0) {
				länge++;

			}
		}

		int[] teilbareGanzzahlen = new int[länge];
		for(int i = startwert; i <= endwert; i++) {
			if(i % n == 0) {
				teilbareGanzzahlen[index] = i;
				index++;
			}
		}
		return teilbareGanzzahlen;	
	}
}


