package uniVerschiedenes;

public class Lottozahlen {

	public static void main(String[] args) {
		
		int[] lottozahlen = new int[6];

		
		for(int i = 0; i<lottozahlen.length; i++) {						// Generiert neue Zufallszahl
			lottozahlen[i] = (int) (Math.random() * 49 + 1);
				
			for(int j = 0; j < i; j++) {								// Wenn der Inhalt des Arrays an der Stelle i = dem Inhalt des Arrays an der Stelle j ist, 
				if(lottozahlen[j] == lottozahlen[i]) {					// dann wird i um 1 verringert und es muss eine zusätzliche Zufallszahl generiert werden 
					i--; 
					break;
				}
			}
		}
		
		
		
		System.out.println("Die gezogenen Lottozahlen lauten: ");
		
		for(int i = 0; i < 6; i++) {
			System.out.println((i+1) + " : " + lottozahlen[i]);
		}

	}

}
