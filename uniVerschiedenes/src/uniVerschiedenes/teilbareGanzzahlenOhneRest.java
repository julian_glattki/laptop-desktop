package uniVerschiedenes;

public class teilbareGanzzahlenOhneRest {

	public static void main(String[] args) {
	
	for(int element : teilbareGanzzahlen(2, 24, 3) ) {
		System.out.println(element);
	}
	}
	
	public static int[] teilbareGanzzahlen(int startwert, int endwert, int n) {
		
		int index = 0;
		
		int[] teilbareGanzzahlen = new int[((endwert-startwert+1)/n)];
		
		for(int i = startwert; i <= endwert; i++) {
			if(i % n == 0) {
				teilbareGanzzahlen[index] = i;
				index++;
			}
		}
		
		return teilbareGanzzahlen;
	}

}
