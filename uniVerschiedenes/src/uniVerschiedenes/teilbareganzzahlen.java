package uniVerschiedenes;

public class teilbareganzzahlen {
	
	public static void main(String[] args) {
		
		int[] teilbareGanzzahlen = teilbareGanzzahlen(0, 25, 3);
		
		for(int element : teilbareGanzzahlen) {
			System.out.println(element);
		}
		
	}
	
	public static int[] teilbareGanzzahlen(int startwert, int endwert, int teiler) {
		int länge = 0;
		int index= 0;
		
		for(int i = startwert; i <= endwert; i++) {
			if(i % teiler == 0) {
				länge++;
			}
		}
		
		int[] teilbareGanzzahlen = new int[länge];
		
		for(int i = startwert; i <= endwert; i++) {
			if(i % teiler == 0) {
				teilbareGanzzahlen[index] = i;
				index++;
			}
		}
		return teilbareGanzzahlen;
	}
}
