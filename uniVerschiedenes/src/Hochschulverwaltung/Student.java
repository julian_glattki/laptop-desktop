package Hochschulverwaltung;

public class Student extends Mensch{
	private String studiengang;
	private int matrikelnummer;

	public Student(String familienname, String vorname, String studiengang, int matrikelnummer) {
		super(familienname, vorname);
		this.studiengang = studiengang; 
		this.matrikelnummer = matrikelnummer; 
	}
	
	public String toString() {
		return super.toString() +  " (" + this.matrikelnummer +")"+ ".Studiengang: " + this.studiengang + ".";
	}
}
