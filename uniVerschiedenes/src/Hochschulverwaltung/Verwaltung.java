package Hochschulverwaltung;

import java.util.ArrayList;

public class Verwaltung {
	ArrayList<Mensch> al = new ArrayList<Mensch>();
	
	public static void main(String[] args) {
		Verwaltung hochschule = new Verwaltung();
		hochschule.erzeugeBeispiele();
		
		System.out.println(hochschule.sucheMitFamiliennamen("Mueller"));
	}
	public void erzeugeBeispiele() {
		al.add(new Professor("Mueller", "Thomas", "Mathematik", "A222"));
		al.add(new Professor("Meier", "Hans", "Biologie", "B333"));
		al.add(new Professor("Bretter", "Peter", "Astronomie", "Z444"));
		al.add(new Professor("Petter", "Pepp", "Toxikologie", "A254"));
		
		al.add(new Student("Mustermann", "Max", "BWL", 123456));
		al.add(new Student("Mueller", "Muss", "VWL", 123446));
		al.add(new Student("Kann", "Nix", "Soziale Arbeit", 123436));
		al.add(new Student("Permaban", "Ella", "Informatik", 123457));
	}
	
	public String sucheMitFamiliennamen(String gesucht) {
		String inFrage = "In Frage kommen:\n"; 
		
		for(Mensch mensch : al) {
			if(mensch.familienname.equals(gesucht)) {
				inFrage += mensch.toString() + "\n";
			}
		}
		
		return inFrage;
	}
}
