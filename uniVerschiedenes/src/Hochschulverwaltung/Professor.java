package Hochschulverwaltung;

public class Professor extends Mensch {
	private String fachgebiet;
	private String buero;
	
	public Professor(String familienname, String vorname, String fachgebiet, String buero) {
		super(familienname, vorname);
		this.fachgebiet = fachgebiet; 
		this.buero = buero; 
	}
	
	public String toString() {
		return super.toString() +  " (" + this.fachgebiet +")"+ ". B�ro: " + this.buero + ".";
	}

}
