package Hochschulverwaltung;

public class Mensch {
	public String familienname; 
	public String vorname;
	
	public Mensch(String familienname, String vorname) {
		this.familienname = familienname;
		this.vorname = vorname;
	}
	
	public String toString() {
		return this.familienname + " " + this.vorname;
	}

}
