package stack;

public class Kellerspeicher {
	private int[] stack; 
	private int size;
	
	public Kellerspeicher(int size) {
		this.size = size;
		this.stack = new int[size];
	}
	
	public boolean isEmpty() {
		for(int i = 0; i < this.size; i++) {
			if(this.stack[i] != 0) {
				return false;
			}
		}
		return true;
	}
	
	public int amountOfNumbers() {
		int amount = 0;
		
		for(int i = 0; i < this.size; i++) {
			if(stack[i] != 0) {
				amount++;
			}
		}
		
		return amount;
	}
	
	public void addElement(int element) {
		if(this.size == this.amountOfNumbers()) {
			throw new RuntimeException("StackOverflowError111111!!!!!!");
		}
		else {
			this.stack[this.amountOfNumbers()] = element;
		}
	}
	
	public void removeElement() {
		if(this.amountOfNumbers() == 0) {
			throw new RuntimeException("Stack ist bereits leer!");
		}
		this.stack[this.amountOfNumbers()-1] = 0;
	}
	
	public static void main(String[] args) {
		Kellerspeicher test = new Kellerspeicher(10);
		for(int i = 1; i < test.size+1; i++) {
			test.addElement(i);
		}
		
		
		test.removeElement();
		test.removeElement();
		test.removeElement();
		test.removeElement();
		test.removeElement();
		test.removeElement();
		test.removeElement();
		test.removeElement();
		test.removeElement();
		test.removeElement();
		
		System.out.println(test.isEmpty());
		
		test.removeElement();

	}

}
