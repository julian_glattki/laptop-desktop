package stack;

public class stackspeicher {
	int[] speicherplaetze;
	int oben = 0; 
	
	public static void main(String[] args) {
		stackspeicher stack = new stackspeicher(10);
		stack.pop();
	
		
	}
	public stackspeicher(int größe) {
		this.speicherplaetze = new int[größe];
	}
	
	public void push(int number) {
		if(this.elementsOnStack() == speicherplaetze.length) {
			System.err.println("Stack fully used");
			return;
		}
		this.speicherplaetze[oben++] = number;
	}
	
	public void pop() {
		if(this.elementsOnStack() == 0) {
			System.err.println("Stack is empty");
			return;
		}
		
		this.speicherplaetze[oben--] = 0;
	}
	
	public boolean isEmpty() {
		return oben == 0;
	}
	
	public int elementsOnStack() {
		return oben;
	}
}
