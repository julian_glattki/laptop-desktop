package wuerfel;

public class Wuerfel {
	int[] seiten;
	
	public Wuerfel(int seiten) {
		int index = 0;
		this.seiten = new int[seiten];
		for(int i = 1; i <= seiten ; i++) {
			this.seiten[index] = i;
			index++;
		} 
	}
	
	public int wuerfeln() {
		int random = ((int) (Math.random()*6));
		
		return this.seiten[random];
	}
}	
