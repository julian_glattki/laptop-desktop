package wuerfel;

public class Wuerfelbecher {
	public Wuerfel[] wuerfel;

	public Wuerfelbecher(int anzahl) {
		this.wuerfel = new Wuerfel[anzahl];

		for(int i = 0; i < this.wuerfel.length; i++) {
			wuerfel[i] = new Wuerfel(6);
		}
	}

	public int[] wuerfeln() {
		int[] geworfen = new int[this.wuerfel.length];

		for(int i = 0; i < geworfen.length; i++) {
			geworfen[i] =  this.wuerfel[i].wuerfeln();
		}

		return geworfen;
	}
	public static void main(String[] args) {
		Wuerfelbecher wuerfel = new Wuerfelbecher(5);

		int[] geworfen = wuerfel.wuerfeln();
		System.out.println(geworfen);
		System.out.println(gleicheAugenzahl(geworfen));

	}

	public static int gleicheAugenzahl(int[] geworfen) {
		int index = 0; 
		int gleich = 0;
		int zähler = 0;
		for(int i = 1; i <= 6; i++) {
		for(int element : geworfen ) {
				if(element == i) {
					zähler++;
				}
			}
		if(zähler >= 2) {
			gleich += zähler;
		}
		zähler = 0;
		}
		
		return gleich;
	}

}
