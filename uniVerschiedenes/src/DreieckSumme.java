import java.io.File;
import java.util.Scanner;

public class DreieckSumme {

	public static void main(String[] args) throws Exception {
		//		int[][] dreieck = dreieckAnlegen(4);
		int[][] dreieck = dreieckLesen(15);

		dreieckAusgeben(dreieck);

		System.out.println();

		int pfad[] = groessteSummeFinden(dreieck);

		System.out.println();
		System.out.println("Pfad der groessten Summe:");
		for (int z = 0; z < dreieck.length; z++) {
			System.out.println(dreieck[z][pfad[z]]);
		}

	}

	static int[][] dreieckAnlegen(int zeilen) {
		int[][] dreieck = new int[zeilen][zeilen];

		for (int i = dreieck.length - 1; i >= 0; i--) {
			for (int j = i; j >= 0; j--) {
				dreieck[i][j] = (int) (Math.random() * 100);
			}
		}

		return dreieck;
	}

	static void dreieckAusgeben(int[][] dreieck) {
		for (int z = 0; z < dreieck.length; z++) {

			for (int s = 0; s < (dreieck.length - z - 1) ; s++) {
				System.out.print("  ");
			}

			for (int y = 0; y <= z; y++) {
				String out = "" + dreieck[z][y];
				out = "00".substring(out.length()) + out;
				System.out.print(out + "  ");

			}
			System.out.println();
		}
	}
	
	// Könnte man sicher noch mit der Methode davor integrieren...
	static void dreieckAusgeben2(int[][] dreieck) {
		for (int z = 0; z < dreieck.length; z++) {

			for (int s = 0; s < (dreieck.length - z - 1) ; s++) {
				System.out.print("   ");
			}

			for (int y = 0; y <= z; y++) {
				String out = "" + dreieck[z][y];
				out = "0000".substring(out.length()) + out;
				System.out.print(out + "  ");

			}
			System.out.println();
		}
	}

	static int[] groessteSummeFinden(int[][] dreieck) {
		// produziert leider nur eine "shallow copy" für 2D-Arrays :-(
		//		int neuesDreieck[][] = Arrays.copyOf(dreieck, dreieck.length);

		int[][] neuesDreieck = new int[dreieck.length][dreieck.length];
		for (int z = 0; z < dreieck.length; z++) {
			for (int s = 0; s < dreieck.length; s++) {
				neuesDreieck[z][s] = dreieck[z][s];
			}
		}

		for (int z = neuesDreieck.length - 2; z >= 0; z--) {
			for (int s = 0; s < z + 1; s++) {
				int links = neuesDreieck[z][s] + neuesDreieck[z+1][s];
				int rechts = neuesDreieck[z][s] + neuesDreieck[z+1][s+1];

				neuesDreieck[z][s] = Math.max(links, rechts);
			}
		}

		System.out.println(neuesDreieck[0][0]);
		System.out.println();
		dreieckAusgeben2(neuesDreieck);
		System.out.println();
		
		int[] pfad = new int[dreieck.length];
		pfad[0] = 0;
		for (int z = 1; z < dreieck.length; z++) {

			int summeVorher = neuesDreieck[z-1][pfad[z-1]];
			int zahlVorher = dreieck[z-1][pfad[z-1]];
			int find = summeVorher - zahlVorher;

			for (int i = 0; i <= z; i++) {
				if (find == neuesDreieck[z][i]) {
					pfad[z] = i;
					break;
				}
			}

		} // for z

		return pfad;
	}

	static int[][] dreieckLesen(int zeilen) throws Exception {
		int[][] dreieck = new int[zeilen][zeilen];
		Scanner sc = new Scanner(new File("resources/dreieck.txt"));

		for (int i = 0; i < zeilen; i++) {
			String in = sc.nextLine();
			String[] zahlen = in.split(" ");

			for (int j = 0; j < zahlen.length; j++) {
				dreieck[i][j] = Integer.parseInt(zahlen[j]);
			}
		}

		sc.close();
		
		return dreieck;
	}
}