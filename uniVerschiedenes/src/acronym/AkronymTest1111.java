package acronym;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;


public class AkronymTest1111 {
	public static final String wordlistPath = "C:\\Users\\julia\\Desktop\\Workspace Git\\uniVerschiedenes\\src\\wortliste.txt";
	public static final int filterMaxLength = 10;
	
	public static void main(String[] args) throws MalformedURLException, IOException {
		ArrayList<String> wordlist = readWordList();
		
		for(String element : findPossibleWords(wordlist, "GER")) {
			System.out.println(highlightAcronym("GER", element));
		}
		
	}


	public static String highlightAcronym(String acronym, String word) {
		int index = 0; 
		String highlighted = ""; 
		for(int i = 0; i < word.length(); i++) {
			if(word.charAt(i) == acronym.charAt(index)) {
				index++; 
				highlighted += "*" + word.charAt(i) + "*";
				if(index == acronym.length()) {
					break;
				}
				continue;

			}
			highlighted += word.charAt(i);
		}
		
		return highlighted; 
	}
	public static ArrayList<String> findPossibleWords(ArrayList<String> wordlist, String acronym) {
		int index = 0; 
		boolean elementAdded = false; 
		ArrayList<String> possibleWords = new ArrayList<String>();
		for(String element : wordlist) {
			for(int i = 0; i < element.length(); i++) {
				
				if(element.charAt(i) == acronym.charAt(index)) {
					index++;
				}
				if(index == acronym.length()) {
					possibleWords.add(element);
					break;
				}
			}
			index = 0; 
		}

		return possibleWords;
	}
	public static ArrayList<String> readWordList() throws MalformedURLException, IOException {
		ArrayList<String> wordlist = new ArrayList<String>();
		Scanner sc = new Scanner(new URL("http://www.netzmafia.de/software/wordlists/deutsch.txt").openStream());

		while(sc.hasNextLine()) {
			String word = sc.nextLine();
			if(word.length() < filterMaxLength) {
				wordlist.add(word.toUpperCase());
			}
		}
		return wordlist;


	}
	public static String generateAcronymFirstLetter(String input) {
		String[] words = input.split(" ");
		String acronym = ""; 

		for(int i = 0; i < words.length; i++){
			acronym += words[i].charAt(0);
		}
		acronym = acronym.toUpperCase();

		return acronym;
	}

	public static String generateAcronymUppercaseLetters(String input) {
		String[] words = input.split(" ");
		String acronym = ""; 

		for(int i = 0; i < words.length; i++){
			for(int j = 0; j < words[i].length(); j++) {
				if(words[i].charAt(j) >= 65 && words[i].charAt(j) <= 90) {
					acronym += words[i].charAt(j);
				}
			}
		}
		acronym = acronym.toUpperCase();

		return acronym;
	}
}
