
package test;

import javax.swing.*;
import java.awt.*;

public class StandardLayout {
	JFrame frame;
	JPanel panel;

	public StandardLayout() {
		createView();

		frame.setTitle("Flow Layout");
		frame.setSize(500,200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setVisible(true);
	}

	public void createView() {
		frame = new JFrame();
		panel = new JPanel();
		frame.getContentPane().add(panel);
	}


	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new FlowLayoutTutorial();
			}
		});
	}
}


