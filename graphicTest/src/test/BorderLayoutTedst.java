package test;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import test.TextArea;

public class BorderLayoutTedst implements ActionListener {
	private JFrame frame; 
	private JLabel label; 
	private JTextArea textarea; 
	private JButton buttonCount, buttonReset; 
	private JPanel panel; 
	
	private static int counter = 1; 
	
	public BorderLayoutTedst() {
		createView();
		
		label = new JLabel("Please click the button!"); 

		
		textarea = new JTextArea(15,35);
		textarea.setLineWrap(true);
		textarea.setWrapStyleWord(true);
		JScrollPane sp = new JScrollPane(textarea);
		sp.setPreferredSize(new Dimension(350,90));

		
		buttonCount = new JButton("Click"); 
		buttonCount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textarea.setText(textarea.getText() + counter + "|");
				counter++;
				
			}
			
		});
		buttonReset = new JButton("Reset");
		buttonReset.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				textarea.setText("");
				counter = 0; 
				
			}
			
		});
		panel.add(label);
		panel.add(buttonCount);
		panel.add(textarea);
		panel.add(buttonReset);
		
		
		
		
		
		frame.setVisible(true);
	}
	
	public void createView() {
		frame = new JFrame(); 
		frame.setSize(400,400);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		panel = new JPanel();
		frame.getContentPane().add(panel);
		frame.setTitle("Test");
		
		
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new BorderLayoutTedst();
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
