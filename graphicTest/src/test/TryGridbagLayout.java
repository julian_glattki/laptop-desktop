package test;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class TryGridbagLayout {
	JFrame frame;
	JPanel panelMain, panelForm;

	public TryGridbagLayout() {
		createView();

		frame.setTitle("Flow Layout");
		frame.setSize(500,200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setVisible(true);
	}

	public void createView() {
		frame = new JFrame();
		panelMain = new JPanel();
		frame.getContentPane().add(panelMain);
		
		panelForm = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0; 
		c.gridy = 0; 
		c.anchor = GridBagConstraints.LINE_END;
		panelForm.add(new JLabel("First Name: "), c);
		c.gridy++;
		
		panelForm.add(new JLabel("Last Name: "), c);
		c.gridy++;
		
		panelForm.add(new JLabel("Email: "), c);
		c.gridy++;
		
		panelForm.add(new JLabel("Username: "), c);
		c.gridy++;
		
		panelForm.add(new JLabel("Password: "), c);
		
		
		c.gridy = 0; 
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		panelForm.add(new JTextField(8), c);
		c.gridy++;
		
		panelForm.add(new JTextField(8), c);
		c.gridy++;
		
		panelForm.add(new JTextField(15), c);
		c.gridy++;
		
		panelForm.add(new JTextField(4), c);
		c.gridy++;
		
		panelForm.add(new JTextField(8), c);
		c.gridy++;

		panelMain.add(panelForm);
	}


	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new TryGridbagLayout();
			}
		});
	}
}
