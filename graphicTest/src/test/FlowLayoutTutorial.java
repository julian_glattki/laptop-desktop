package test;

import javax.swing.*;
import java.awt.*;

public class FlowLayoutTutorial {
	JFrame frame;
	JPanel panelMain;
	JLabel label1, label2;
	JPasswordField passwordField;
	JTextField fieldUsername; 
	
	public FlowLayoutTutorial() {
		createView();
		
		frame.setTitle("Flow Layout");
		frame.setSize(500,200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setVisible(true);
	}
	
	public void createView() {
		frame = new JFrame();
		panelMain = new JPanel(new FlowLayout());
		frame.getContentPane().add(panelMain);
		
		label1 = new JLabel("Username: ");
		panelMain.add(label1);
		
		fieldUsername = new JTextField(20);
		panelMain.add(fieldUsername);
		
		label2 = new JLabel("Password: ");
		panelMain.add(label2);
		
		passwordField = new JPasswordField(20);
		panelMain.add(passwordField);		
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new FlowLayoutTutorial();
			}
		});
	}
}
