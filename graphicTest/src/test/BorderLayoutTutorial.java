package test;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import test.TextArea;

public class BorderLayoutTutorial implements ActionListener{
	private JFrame frame; 
	private JTextField fieldMessage; 
	private JTextArea textArea; 
	private JButton buttonSubmit, buttonClear; 
	private JPanel panel, panelNorth; 

	private static int counter = 1; 

	public BorderLayoutTutorial() {
		createView();


		frame.setTitle("aaa");
		frame.setSize(400,400);
		frame.setMinimumSize(new Dimension(400,200));
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void createView() {
		frame = new JFrame(); 
		panel = new JPanel();
		panel.setBorder(new EmptyBorder(10,10,10,10));
		panel.setLayout(new BorderLayout());
		frame.getContentPane().add(panel);
		
		// North
		panelNorth = new JPanel(new BorderLayout());
		panel.add(panelNorth, BorderLayout.NORTH);
		panelNorth.add(new JLabel("Enter a message"), BorderLayout.WEST);
		
		fieldMessage = new JTextField(); 
		panelNorth.add(fieldMessage, BorderLayout.CENTER);
		
		buttonSubmit = new JButton("Submit"); 
		panelNorth.add(buttonSubmit, BorderLayout.EAST);
		buttonSubmit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				textArea.append(fieldMessage.getText() + "\n");
				
				
			}
			
		});
		//
		
		//Center
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		JScrollPane scrollPane = new JScrollPane(textArea);
		panel.add(scrollPane);
		
		//South
		JPanel panelSouth = new JPanel();
		panel.add(panelSouth, BorderLayout.SOUTH);
		
		buttonClear = new JButton("Clear Text");
		panelSouth.add(buttonClear);
		buttonClear.addActionListener(new ActionListener () {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				textArea.setText("");
				
			}
			
		});
		//
		


	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new BorderLayoutTutorial();
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}
}
