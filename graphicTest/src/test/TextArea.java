package test;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.SwingUtilities;

public class TextArea extends JFrame{
	private JTextArea textArea;
	private JButton buttonClear;
	
	private JTextField fieldMessage;
	private JButton buttonSubmit;
	
	public TextArea() {
		createView();
		setTitle("Text Area Test");
		setSize(400,225);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
	}
	
	public void createView() {
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		
		JLabel label = new JLabel("Enter some text:");
		panel.add(label);
		
		fieldMessage = new JTextField(12);
		panel.add(fieldMessage);
		
		buttonSubmit = new JButton("Submit");
		panel.add(buttonSubmit);
		buttonSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String message = fieldMessage.getText();
				if(!message.isEmpty()) {
					textArea.append(message + "\n");
				}
				fieldMessage.setText("");
			}
		});
		
		// Create TextArea
		textArea = new JTextArea();
//		textArea.setVisible(true);
		textArea.setEditable(false);
		// Go to next line if area is full
		textArea.setLineWrap(true);
		// Shift entire word to next line instead of only a few letters
		textArea.setWrapStyleWord(true);
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setPreferredSize(new Dimension(350,90));
		panel.add(scrollPane);
		
		buttonClear = new JButton("Clear Text Area");
		buttonClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textArea.setText("");
				
			}
			
		});
		panel.add(buttonClear);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new TextArea().setVisible(true);;
			}
		});
	}

	

}
